# README #

### What is this repository for? ###

* This application helps with planning trips on marked trails in Poland.
* Version 0.1

### How do I get set up? ###

* Create the TrekkingGps.apk file and copy it with files from link https://www.dropbox.com/sh/yddwvf1dqm8byf9/AADEu2JqYxuzVQOFzCHq4Fjma?dl=0 to SD card of your device.
* Install application on device and it should work!