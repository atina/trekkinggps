package jsqlite;

import java.io.File;

import android.app.Activity;
import android.os.Bundle;

public class CreateWayPos extends Activity {

	private static String DB_NAME = "mergedWithMulti.osm.db";

	protected static Database db;

	public static void openSpatialite() {
		String path = "C:\\Users\\Anita\\Downloads\\osmosis-latest\\bin\\";
		File spatialDbFile = new File(path, DB_NAME);

		db = new jsqlite.Database();
		try {
			db.open(spatialDbFile.getAbsolutePath(), jsqlite.Constants.SQLITE_OPEN_READWRITE | jsqlite.Constants.SQLITE_OPEN_CREATE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void closeSpatialite() {
		if (db != null)
			try {
				db.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		openSpatialite();

		String query = "select count(pkuid) from allintersectionswithblue";

		Integer pointCount = null;
		try {
			Stmt stmt = db.prepare(query);

			if (stmt.step()) {

				pointCount = stmt.column_int(0);

			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int i = 0; i < pointCount; i++) {
			query = "select X(Geometry), Y(Geometry) from allintersectionswithblue where PKUID=" + i + 1;

			Double x = null, y = null;
			try {
				Stmt stmt = db.prepare(query);

				if (stmt.step()) {

					x = stmt.column_double(0);
					y = stmt.column_double(1);

				}
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			query = "replace into allintersectionswithblue select pkuid, Geometry, id_1, id_2,(select avg(way_pos) from ( select way_pos ,ST_Distance(MakePoint( "
					+ x
					+ " , "
					+ y
					+ " ), geometry) as distance from ways_nodes join nodes where node_id=nodes.id and way_id=(select id from(SELECT id , geometry,ST_Distance(MakePoint( "
					+ x
					+ " , "
					+ y
					+ " ), geometry) as distance  FROM mergedWithMulti_polylines WHERE  ROWID IN ( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'mergedWithMulti_polylines' AND search_frame = BuildCircleMbr( "
					+ x
					+ " , "
					+ y
					+ " ,0.1)) order by distance limit 1)) order by distance limit 2)) as avg_way_pos from allintersectionswithblue where pkuid =1";
		}
		closeSpatialite();
	}
}
