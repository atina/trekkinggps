package com.example.trekkinggps.routesfound;

import java.sql.SQLException;

import android.app.*;
import android.view.LayoutInflater;
import android.widget.*;
import com.example.trekkinggps.commons.LocationUpdatesConstants;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.Editable;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.trekkinggps.R;
import com.example.trekkinggps.databases.service.FoundRouteService;
import com.example.trekkinggps.findroute.model.FoundRoute;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

/**
 * Klasa odpowiedzialna za reakcje na klikniecie na numer znalezionej trasy po
 * jej wybraniu pokazuje znaleziona trase
 */
public class FoundRouteClickListener implements ListView.OnItemClickListener,
		OnClickListener {

	private final Activity context;
	private final MapView mapView;
	private final FoundRoute foundRoute;
	private final ListView descriptionView;
	private final ImageButton hideDescriptionButton;
	private RouteStatistics routeStatistics = null;
	private boolean isRouteStarted = false;
	private ImageButton alertView;
	private MyLocationNewOverlay myLocationOverlay;
	private TableLayout statsView;

	public FoundRouteClickListener(final Activity context, MapView mapView,
			FoundRoute foundRoute) {
		this.context = context;
		this.mapView = mapView;
		this.foundRoute = foundRoute;
		this.descriptionView = (ListView) context
				.findViewById(R.id.routes_description);
		this.hideDescriptionButton = (ImageButton) context
				.findViewById(R.id.hide_button);
		this.hideDescriptionButton.setOnClickListener(this);
		this.alertView = (ImageButton) context.findViewById(R.id.off_trail);
		alertView.setOnClickListener(generateAlertButtonClickListener());
		statsView = (TableLayout) context.findViewById(R.id.route_stats);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		selectItem(position, parent);

	}

	// pokazuje na ekranie wybran� tras�
	private void selectItem(int position, AdapterView<?> view) {
		if (position == 0) {
			showOrHideDescriptionWindow();
		} else if (position == 1) {
			double lon = foundRoute.getFullPointsList().get(0).getLon();
			double lat = foundRoute.getFullPointsList().get(0).getLat();
			GeoPoint point = new GeoPoint(lat, lon);

			mapView.getController().animateTo(point);
		} else if (position == 2) {
			// save found route to database
			AlertDialog.Builder alert = createPromptForName();

			alert.show();

		} else if (position == 3) {

			//gets menu adapter and switches its icon between start trail and stop trail
			FoundRoutesAdapter menuAdapter = (FoundRoutesAdapter) view.getAdapter();

			LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

			if(menuAdapter.getItem(position).equals("StartTrail")) {

				//trail was just started (start icon was clicked)
				//icon is set to stop trail (if we want to stop our current trail)
				menuAdapter.setValue(position, "StopTrail");

				//adds gps icon to map
				myLocationOverlay = new MyLocationNewOverlay(this.context, mapView);
				myLocationOverlay.enableMyLocation();
				myLocationOverlay.enableFollowLocation();
				mapView.getOverlays().add(myLocationOverlay);
				mapView.invalidate();

				//starts gps
				lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, LocationUpdatesConstants.MINIMUM_TIME, LocationUpdatesConstants.MINIMUM_DISTANCE,
						locationListener);

			} else {

				//trail was just stopped (stop icon was clicked)
				//icon is set to start trail (if we want to start trail again)
				menuAdapter.setValue(position, "StartTrail");

				//clear all statistics, off trail data and gps settings because trail has been already cancelled
				mapView.getOverlays().remove(myLocationOverlay);
				mapView.invalidate();
				foundRoute.clearOffTrailPoints();
				alertView.setVisibility(View.GONE);
				statsView.setVisibility(View.GONE);
				isRouteStarted = false;

				//stops gps updates
				lm.removeUpdates(this.locationListener);

			}

		}

	}

	private AlertDialog.Builder createPromptForName() {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);

		alert.setTitle("Save route");
		alert.setMessage("Enter name for route");

		// Set an EditText view to get user input
		final EditText input = new EditText(context);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
				saveRoute(input);
			}

		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});
		return alert;
	}

	private void saveRoute(final EditText input) {
		Editable value = input.getText();

		FoundRouteService service = new FoundRouteService(context);
		try {
			service.save(foundRoute, value.toString());
			Toast.makeText(context, "Route successfully saved",
					Toast.LENGTH_LONG).show();
		} catch (SQLException e) {
			Toast.makeText(context, "Error occured during save",
					Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	private void showOrHideDescriptionWindow() {

		RelativeLayout.LayoutParams alertViewLayoutParams = (RelativeLayout.LayoutParams) alertView.getLayoutParams();

		if (descriptionView.getVisibility() == View.VISIBLE) {
			descriptionView.setVisibility(View.GONE);
			hideDescriptionButton.setImageResource(R.drawable.ic_hide_right);
			alertViewLayoutParams.addRule(RelativeLayout.BELOW, 0);
		} else {
			descriptionView.setVisibility(View.VISIBLE);
			hideDescriptionButton.setImageResource(R.drawable.ic_hide_left);
			alertViewLayoutParams.addRule(RelativeLayout.BELOW, R.id.hide_button);
		}

		alertView.setLayoutParams(alertViewLayoutParams);
	}

	// funkcja odpowiedzialna za pokazywanie/chowanie panelu z opisem tras
	@Override
	public void onClick(View arg0) {
		showOrHideDescriptionWindow();
	}

	// listener kt�ry reaguje na zmiany lokalizacji i w przypadku jej wykrycia
	// od�wie�a bie��c� lokalizacj�
	private final LocationListener locationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLocationChanged(Location location) {
			updateLocation(location);

		}

	};

	private void updateLocation(Location location) {
		GeoPoint geoPoint = new GeoPoint(location.getLatitude(),
				location.getLongitude());
		mapView.getController().animateTo(geoPoint);

		if (foundRoute.isPointOnRoute(geoPoint)) {
			foundRoute.clearOffTrailPoints();
			alertView.setVisibility(View.GONE);
		} else {
			foundRoute.calculateOffTrailPoints(geoPoint);
			alertView.setVisibility(View.VISIBLE);
			showNotification();
		}

		mapView.invalidate();

		if (!isRouteStarted) {

			String[] time = foundRoute.getTime().split(" ");
			Time timeToEnd = new Time();
			timeToEnd.hour = Integer.valueOf(time[0].substring(0,
					time[0].length() - 1));
			timeToEnd.minute = Integer.valueOf(time[1].substring(0,
					time[1].length() - 1));

			routeStatistics = new RouteStatistics(geoPoint, timeToEnd, context);
			isRouteStarted = true;
		} else {

			routeStatistics.setTrailTime(calculateNewTrailTime());
			routeStatistics.setTrailDistance(routeStatistics.getTrailDistance()
					+ geoPoint.distanceTo(routeStatistics
							.getCurrentCoordinates()));
			routeStatistics.setCurrentCoordinates(geoPoint);

			routeStatistics.setTrailTimeToEnd(foundRoute.getDistance());

			Time expectedTime = calculateTime(routeStatistics
					.getTrailDistance());
			routeStatistics.setExpectedTime(expectedTime);
			int diff = Time.compare(routeStatistics.getTrailTime(),
					expectedTime);

			if (diff > 0) {
				routeStatistics.matchRateOfGoing(false);
			} else {
				routeStatistics.matchRateOfGoing(true);
			}

		}

		routeStatistics.updateStatistics();
		statsView.setVisibility(View.VISIBLE);

	}

	private Time calculateNewTrailTime() {
		Time now = new Time();
		now.setToNow();

		long differenceInMilis = now.toMillis(false)
				- routeStatistics.getCurrentTime().toMillis(false);

		int diffSeconds = (int) (differenceInMilis / 1000 % 60);
		int diffMinutes = (int) (differenceInMilis / (60 * 1000) % 60);
		int diffHours = (int) (differenceInMilis / (60 * 60 * 1000) % 24);

		routeStatistics.setCurrentTime(now);

		Time newTrailTime = new Time();
		int newSeconds = routeStatistics.getTrailTime().second + diffSeconds;
		int newMinutes = routeStatistics.getTrailTime().minute + diffMinutes;
		int newHours = routeStatistics.getTrailTime().hour + diffHours;

		newTrailTime.second = newSeconds % 60;
		newTrailTime.minute = (newMinutes + newSeconds / 60) % 60;
		newTrailTime.hour = newHours + (newMinutes + newSeconds / 60) / 60;

		return newTrailTime;
	}

	private Time calculateTime(double length) {
		double lengthInKm = length / 1000;
		double timeInMinutes = lengthInKm * 15;
		int hours = (int) (Math.floor(timeInMinutes) / 60);
		int minutes = (int) (Math.floor(timeInMinutes) % 60);
		int seconds = (int) Math.floor((timeInMinutes - Math
				.floor(timeInMinutes)) * 60);

		Time time = new Time();
		time.hour = hours;
		time.minute = minutes;
		time.second = seconds;

		return time;
	}


	/**
	 * Sends notification to system, when user is off trail
	 */
	private void showNotification() {

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.ic_offtrail)
				.setContentTitle("TrekkingGPS - Found Route")
				.setContentText("You are off the trail!");

		Intent notificationIntent = new Intent();

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);

		builder.setContentIntent(contentIntent);
		builder.setAutoCancel(true);
		long[] pattern = { 1000, 1000, 1000, 1000, 1000 };
		builder.setVibrate(pattern);
		builder.setStyle(new NotificationCompat.InboxStyle());
		builder.setSound(RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

		NotificationManager manager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.notify(1, builder.build());

	}


	/**
	 * generates on click listener for off trail alert button (makes that button shows popup)
	 * @return Button.OnClickListener for alert button
	 */
	public Button.OnClickListener generateAlertButtonClickListener() {
		return new Button.OnClickListener(){
			@Override
			public void onClick(View arg0) {
				LayoutInflater layoutInflater = (LayoutInflater) context.getBaseContext().getSystemService(context.LAYOUT_INFLATER_SERVICE);
				View popupView = layoutInflater.inflate(R.layout.popup, null);
				final PopupWindow popupWindow = new PopupWindow(popupView, ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

				ImageButton buttonClose = (ImageButton) popupView.findViewById(R.id.close_alert_text);
				buttonClose.setOnClickListener(new Button.OnClickListener() {
					@Override
					public void onClick(View v) {
						popupWindow.dismiss();
					}
				});
				popupWindow.showAsDropDown(alertView, 50, -30);
			}
		};
	}
}