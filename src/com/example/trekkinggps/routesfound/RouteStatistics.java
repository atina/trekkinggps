package com.example.trekkinggps.routesfound;

import android.app.Activity;
import android.widget.TextView;
import com.example.trekkinggps.R;
import org.osmdroid.util.GeoPoint;

import android.text.format.Time;

public class RouteStatistics {

	private Time currentTime;
	private GeoPoint currentCoordinates;
	private Time trailTime;
	private double trailDistance;
	private Time expectedTime;
	private Time trailTimeToEnd;

	private Activity context;

	//stats views ids
	private TextView time;
	private TextView expected;
	private TextView distance;
	private TextView timeLeft;

	public RouteStatistics(GeoPoint currentCoordinates, Time trailTimeToEnd, Activity context) {
		this.context = context;
		this.currentTime = new Time();
		this.currentTime.setToNow();
		
		this.currentCoordinates = currentCoordinates;
		
		this.trailTime = new Time();
		trailTime.hour = 0;
		trailTime.minute = 0;
		trailTime.second = 0;
		
		this.expectedTime = new Time();
		expectedTime.hour = 0;
		expectedTime.minute = 0;
		expectedTime.second = 0;
		
		this.trailDistance = 0;

		this.trailTimeToEnd = trailTimeToEnd;

		time = (TextView) context.findViewById(R.id.time);
		expected = (TextView) context.findViewById(R.id.expected);
		distance = (TextView) context.findViewById(R.id.distance);
		timeLeft = (TextView) context.findViewById(R.id.time_left);
	}
	
	public Time getExpectedTime() {
		return expectedTime;
	}

	public void setExpectedTime(Time expectedTime) {
		this.expectedTime = expectedTime;
	}

	public Time getCurrentTime() {
		return currentTime;
	}


	public void setCurrentTime(Time currentTime) {
		this.currentTime = currentTime;
	}


	public GeoPoint getCurrentCoordinates() {
		return currentCoordinates;
	}


	public void setCurrentCoordinates(GeoPoint currentCoordinates) {
		this.currentCoordinates = currentCoordinates;
	}


	public Time getTrailTime() {
		return trailTime;
	}


	public void setTrailTime(Time trailTime) {
		this.trailTime = trailTime;
	}


	public double getTrailDistance() {
		return trailDistance;
	}


	public void setTrailDistance(double trailDistance) {
		this.trailDistance = trailDistance;
	}

	public void setTrailTimeToEnd(double totalDistance) {
		//velocity is calculated in meters per second
		double velocity = trailDistance/(trailTime.hour*3600 + trailTime.minute*60 + trailTime.second);
		double remainingDistance = totalDistance - trailDistance;

		double remainingTimeInMinutes = (remainingDistance/velocity)/60;

		int hours = (int) (Math.floor(remainingTimeInMinutes) / 60);
		int minutes = (int) (Math.floor(remainingTimeInMinutes) % 60);
		int seconds = (int) Math.floor((remainingTimeInMinutes - Math.floor(remainingTimeInMinutes)) * 60);

		trailTimeToEnd.hour = hours;
		trailTimeToEnd.minute = minutes;
		trailTimeToEnd.second = seconds;

	}

	public Time getTrailTimeToEnd() {
		return this.trailTimeToEnd;
	}

	public void updateStatistics() {
		time.setText(trailTime.hour + "h " + trailTime.minute + "m " + trailTime.second + "s");
		expected.setText(expectedTime.hour + "h " + expectedTime.minute + "m " + expectedTime.second + "s");
		distance.setText(trailDistance + "m");
		timeLeft.setText(trailTimeToEnd.hour + "h " + trailTimeToEnd.minute + "m ");
	}

	public void matchRateOfGoing(boolean isGood) {
		if(isGood) {
			time.setTextColor(context.getResources().getColor(android.R.color.holo_blue_bright));
		} else {
			time.setTextColor(context.getResources().getColor(android.R.color.holo_red_light));
		}
	}
}
