package com.example.trekkinggps.routesfound;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.trekkinggps.R;

/**
 * klasa wyswietlajaca opisy dla znalezionych tras
 * -szlaki wchodzace w sklad znalezionej trasy
 * -laczny czas
 */
public class RoutesDescriptionAdapter extends ArrayAdapter<String> {
	
	private Context context;
	private List<String> values;
	
	public RoutesDescriptionAdapter(Context context, List<String> values) {
		super(context, R.layout.routesdescription_layout, values);
		this.context = context;
		this.values = values;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.routesdescription_layout, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.route_desc);
		textView.setText(Html.fromHtml(values.get(position)));
	
		
		if(values.get(position).equals("Trails") || values.get(position).equals("Summaric time")) {
			rowView.setBackgroundColor(Color.parseColor("#33B5E5"));
			textView.setTextColor(Color.parseColor("#000000"));
		} else {
			rowView.setBackgroundColor(Color.parseColor("#000000"));
			textView.setTextColor(Color.parseColor("#33B5E5"));
		}
		
		
		return rowView;
	}
	

}
