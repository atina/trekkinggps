package com.example.trekkinggps.routesfound;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.trekkinggps.R;

public class FoundRoutesAdapter extends ArrayAdapter<String> {

	private final Context context;
	private final String[] values;

	public FoundRoutesAdapter(Context context, String[] values) {
		super(context, R.layout.items_layout, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.items_layout, parent, false);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);

		String s = values[position];
		if (s.equals("Description")) {
			imageView.setImageResource(R.drawable.ic_descr_blue);
		} else if (s.equals("Route")) {
			imageView.setImageResource(R.drawable.ic_route);
		} else if (s.equals("Download")) {
			imageView.setImageResource(R.drawable.ic_download);
		} else if (s.equals("StartTrail")) {
			imageView.setImageResource(R.drawable.ic_start_trail);
		} else if(s.equals("StopTrail")) {
			imageView.setImageResource(R.drawable.ic_stop_trail);
		}

		return rowView;
	}


	public void setValue(int position, String value) {
		values[position] = value;
	}

}