package com.example.trekkinggps.commons;

/**
 * Created by Ewelina Swiderska on 2014-12-25.
 * Constants for gps configuration
 * Determines gps updating frequency - both configs must be fulfilled for gps update
 */
public class LocationUpdatesConstants {

    /**
     * Minimal time after which gps is updated (in milliseconds)
     */
    public static final int MINIMUM_TIME = 30000;

    /**
     * Minimal distance after which gps is updated (in meters)
     */
    public static final int MINIMUM_DISTANCE = 5;

}
