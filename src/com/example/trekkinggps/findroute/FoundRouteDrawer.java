package com.example.trekkinggps.findroute;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.MapView.Projection;
import org.osmdroid.views.overlay.Overlay;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

import com.example.trekkinggps.findroute.model.FoundRoute;
import com.example.trekkinggps.findroute.model.Node;

/**
 *  klasa rysujaca znaleziona trase na mapie 
 */
public class FoundRouteDrawer extends Overlay {

	private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private final Path path = new Path();
	private FoundRoute foundRoute;

	public FoundRouteDrawer(Context ctx) {
		super(ctx);
	}

	public FoundRoute getFoundRoute() {
		return foundRoute;
	}

	public void setFoundRoute(FoundRoute foundRoute) {
		this.foundRoute = foundRoute;
	}

	@Override
	protected void draw(Canvas canvas, MapView mapView, boolean arg2) {
		initPaint();
		boolean first = true;
		drawFoudRoute(first, mapView);
		if(foundRoute.isOffTrail()) {
			drawPathToFoundRoute(foundRoute.getOffTrailPoints(),mapView, canvas);
		}
		canvas.drawPath(path, paint);
	}

	private void initPaint() {
		paint.setDither(true);
		paint.setColor(Color.MAGENTA);
		//paint.setAlpha(100);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(6);
	}

	/**
	 * Draws found route without overflow.
	 */
	private void drawFoudRoute(boolean first, MapView arg1) {
		
		final android.graphics.Point p = new android.graphics.Point();
		for (int j = 0; j < foundRoute.getFullPointsList().size(); j++) {
			
			Node node = foundRoute.getFullPointsList().get(j);

			Projection projection = arg1.getProjection();
			GeoPoint point = new GeoPoint(node.getLat(), node.getLon());
			projection.toPixels(new GeoPoint(point.getLatitudeE6(), point.getLongitudeE6()), p);
			if (first) {
				path.rewind();
				path.moveTo(p.x, p.y);
				first = false;
			}
			path.lineTo(p.x, p.y);
		}
	}


	/**
	 * Draws path to found route when we are off the trail
	 * @param offTrailPoints
	 * @param mapView
	 * @param canvas
	 */
	public void drawPathToFoundRoute(OffTrailPoints offTrailPoints, MapView mapView, Canvas canvas) {
		Path closestPath = new Path();
		final android.graphics.Point p = new android.graphics.Point();
		Projection projection = mapView.getProjection();

		projection.toPixels(offTrailPoints.getCurrentLocation(), p);
		closestPath.moveTo(p.x, p.y);
		projection.toPixels(offTrailPoints.getClosestPointOnTrail(), p);
		closestPath.lineTo(p.x, p.y);

		paint.setColor(Color.RED);
		canvas.drawPath(closestPath, paint);
		paint.setColor(Color.MAGENTA);
	}
}
