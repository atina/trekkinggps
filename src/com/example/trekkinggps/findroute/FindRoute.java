package com.example.trekkinggps.findroute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osmdroid.util.GeoPoint;

import android.util.Log;

import com.example.trekkinggps.astar.AStar;
import com.example.trekkinggps.astar.Key;
import com.example.trekkinggps.databases.RouteDatabaseHelper;
import com.example.trekkinggps.findroute.model.FoundRoute;
import com.example.trekkinggps.findroute.model.Node;
import com.example.trekkinggps.findroute.model.Node.Color;
import com.example.trekkinggps.findroute.model.RoutePart;

public class FindRoute {
	private RouteDatabaseHelper database;
	boolean isRoute = false;
	private FoundRoute foundRoute = null;

	public void setDatabase(RouteDatabaseHelper database) {
		this.database = database;
	}

	public FoundRoute getFoundRoute() {
		return foundRoute;
	}

	// Contains the closest nodes for added points - they are not closest for
	// routing nodes.
	private List<Node> pointsOnTrail;
	private boolean overflowOnStart;
	private boolean overflowOnEnd;

	/**
	 * w zaleznosci od tego czy numeracja punktow w szlaku spada czy rosnie
	 * zwraca w odpowiedniej kolejnosci punkty posredniczace miedzy podanymi
	 * dwoma pozycjami na szlaku
	 */
	private List<Node> getBetweenPoints(int wayPos1, int wayPos2, Long wayId) {
		List<Node> pointsBetween = null;
		if (Math.abs(wayPos1 - wayPos2) > 1) {
			if (wayPos1 > wayPos2) {
				pointsBetween = database.getWayPointsBeetweenTwoPoints(wayPos2 + 1, wayPos1 - 1, wayId, true);

			} else {
				pointsBetween = database.getWayPointsBeetweenTwoPoints(wayPos1 + 1, wayPos2 - 1, wayId, false);
			}
		}
		return pointsBetween;
	}

	/**
	 * sprawdza czy dwa punkty sa na tym samym szlaku: jesli tak to w zaleznosci
	 * czy numeracja way_pos w strone drugiego punktu idzie w gore czy w dol to
	 * w takiej kolejnosci zwraca posrednie punkty na trasie miedzy tymi
	 * punktami (to sie dzieje w getBetweenPoints) jesli nie sa na tym samym
	 * szlaku to: szuka przeciecia tych dwoch szlakow dodaje punkty od 1 punktu
	 * do przeciecia, przeciecie i punkty od przeciecia do drugiego punktu
	 * korzystajac znow z getBetweenPoints
	 */
	private void betweenPoints(Node point1, Node point2, List<Node> points) {
		points.add(point1);

		List<Node> pointsBetween = getBetweenPoints(point1.getWayPos(), point2.getWayPos(), point1.getWayId());

		if (pointsBetween != null) {
			// points.add(0,point1);
			points.addAll(pointsBetween);
			// points.add(point2);
		}

		points.add(point2);
	}

	/**
	 * Tworzy z punktow (wyklikanych) liste PointsOnTrail - one obrocz geopointa
	 * maja jeszcze pozycje w szlaku (bo punkty na szlaku sa numerowane tym
	 * way_pos) i identyfikator szlaku pozniej jak jest > 1 punkt to dla kazdej
	 * pary jest wywolywana funkcja between points - opis wyzej na koncu jest
	 * dodawany ostatni punkt
	 */
	public boolean getRoute(List<GeoPoint> addedPoints) {
		// closest points in routing graph
		List<Node> pointsOnTrailForAddedPoints = findPointsOnTrails(addedPoints);
		foundRoute = new FoundRoute();
		List<Node> fullPointsList = findRoute(pointsOnTrailForAddedPoints, foundRoute);

		if (fullPointsList == null) {
			isRoute = false;
		} else {
			foundRoute.setFullPointsList(fullPointsList);
		}
		return isRoute;

	}

	/**
	 * Finds route between <code>addedPoints</code>.
	 *
	 * @param pointsOnTrailForAddedPoints
	 * @param foundRoute
	 * @return
	 */
	private List<Node> findRoute(List<Node> pointsOnTrailForAddedPoints, FoundRoute foundRoute) {
		List<Node> fullPointsList = null;

		if (pointsOnTrailForAddedPoints.size() > 1) {

			fullPointsList = new ArrayList<Node>();

			int i = 0;
			while (i < pointsOnTrailForAddedPoints.size() - 1) {
				AStar.cameFrom = new HashMap<Key, Node>();

				isRoute = AStar.Astar(pointsOnTrailForAddedPoints.get(i), pointsOnTrailForAddedPoints.get(i + 1),
						database);

				if (isRoute) {
					AStar.cameFrom.remove(pointsOnTrailForAddedPoints.get(i).getKey());
					List<Node> foundPath = reconstructPath(AStar.cameFrom, pointsOnTrailForAddedPoints.get(i + 1));

					getPointsFromDb(foundRoute, fullPointsList, foundPath, pointsOnTrail.get(i),
							pointsOnTrail.get(i + 1));
					AStar.cameFrom = new HashMap<Key, Node>();
				}

				i++;
			}
		}
		return fullPointsList;
	}

	private int routePartNumber = -1;

	private void getPointsFromDb(FoundRoute foundRoute, List<Node> fullPointsList, List<Node> foundPath,
			Node startPoint, Node goalPoint) {

		routePartNumber++;
		RoutePart routePart = new RoutePart();
		routePart.setColor(foundPath.get(0).getColor());
		foundRoute.addDistanceMap(routePartNumber, routePart);

		int j = 0;
		overflowOnEnd = false;
		overflowOnStart = false;

		int foundPathSize = fullPointsList.size();

		while (j < foundPath.size() - 1 && !overflowOnEnd) {

			Node point1 = foundPath.get(j);
			Node point2 = foundPath.get(j + 1);

			if (point2.getColor() != null && (routePart.getColor() != point2.getColor())) {
				routePart = new RoutePart();
				routePartNumber++;
				foundRoute.addDistanceMap(routePartNumber, routePart);
				routePart.setColor(point2.getColor());
				Log.d("TrekkingGPS", "new part +" + routePart.getColor() + " point2 kolor " + point2.getColor());
			}

			if (point1.getWayId().equals(point2.getWayId())) {

				List<Node> points = new ArrayList<Node>();
				betweenPoints(point1, point2, points);

				double distance = 0;
				int pointsNumber = 0;
				for (int k = 0; k < points.size() && !overflowOnEnd; k++) {
					pointsNumber++;

					distance += calculateDistance(points, k);

					if (points.get(k).equals(startPoint)) {
						routePartNumber++;
						// foundRoute.clearMap();

						Color color = routePart.getColor();

						routePart = new RoutePart();
						routePart.setColor(color);

						foundRoute.addDistanceMap(routePartNumber, routePart);
						int a = k;
						if (a > 0)
							a = a - 1;
						points = points.subList(a, points.size() - 1);

						overflowOnStart = true;

					} else if (points.get(k).equals(goalPoint)) {
						points = points.subList(0, k + 1);
						overflowOnEnd = true;
					}

				}

				routePart.setCenterPoint(new GeoPoint(points.get(pointsNumber/2).getLat(), points.get(pointsNumber/2).getLon()));
				routePart.setDividerPoint(new GeoPoint(point2.getLat(), point2.getLon()));

				// dodajemy dystans czastkowy i sumujemy do calosciowego
				Log.d("PARTCENTER", routePart.getColor().toString());
				routePart.addDistance(distance);
				foundRoute.addDistance(distance);

				fullPointsList.addAll(points);

			} else {
				fullPointsList.add(point1);
				fullPointsList.add(point2);
			}
			j++;
		}

		if (!overflowOnEnd) {
			List<Node> points = new ArrayList<Node>();

			foundRoute.addDistanceMap(routePartNumber, routePart);

			double distance = calulateDistanceForOverflow(foundPath.get(foundPath.size() - 1), goalPoint, points, routePart);

			foundRoute.addDistance(distance);
			routePart.addDistance(distance);
			routePart.setDividerPoint(new GeoPoint(foundPath.get(foundPath.size() - 1).getLat(), foundPath.get(foundPath.size() - 1).getLon()));

			fullPointsList.addAll(points);
		}
		if (!overflowOnStart) {
			List<Node> points = new ArrayList<Node>();
			routePart = foundRoute.getDistanceMap(0);

			Log.d("TrekkingGPS", "new part +" + routePart.getColor());

			double distance = calulateDistanceForOverflow(startPoint, foundPath.get(0), points, routePart);
			foundRoute.addDistance(distance);
			routePart.addDistance(distance);
			routePart.setDividerPoint(new GeoPoint(foundPath.get(0).getLat(), foundPath.get(0).getLon()));

			// wstawiamy na pocz�tek tego kawa�ka trasy
			fullPointsList.addAll(foundPathSize, points);
		}

	}

	private double calulateDistanceForOverflow(Node point, Node edgePoint, List<Node> points, RoutePart routePart) {

		point.setWayId(edgePoint.getWayId());

		betweenPoints(point, edgePoint, points);

		double distance = 0;
		for (int k = 0; k < points.size(); k++) {
			distance += calculateDistance(points, k);
		}

		GeoPoint centerPoint = new GeoPoint(points.get(points.size()/2).getLat(), points.get(points.size()/2).getLon());
		routePart.setCenterPoint(centerPoint);

		return distance;

	}

	private double calculateDistance(List<Node> points, int k) {
		double distance = 0;
		if (k < points.size() - 1) {
			GeoPoint p1 = new GeoPoint(points.get(k).getLat(), points.get(k).getLon());
			distance += p1.distanceTo(new GeoPoint(points.get(k + 1).getLat(), points.get(k + 1).getLon()));
		}

		return distance;
	}

	/**
	 * Returns appropriet points for <code>addedPoints</code> on routing trail.
	 * 
	 * @param addedPoints
	 * @return
	 */

	private List<Node> findPointsOnTrails(List<GeoPoint> addedPoints) {
		// closest points in routing graph
		List<Node> pointsOnTrailForAddedPoints = new ArrayList<Node>();
		pointsOnTrail = new ArrayList<Node>();
		for (GeoPoint point : addedPoints) {
			Node pointOnTrail = database.getLocationOnTrailForPointRouting(point, pointsOnTrail);
			if (pointOnTrail != null) {
				pointsOnTrailForAddedPoints.add(pointOnTrail);
			}
		}
		return pointsOnTrailForAddedPoints;
	}

	private List<Node> p;

	public List<Node> reconstructPath(Map<Key, Node> cameFrom, Node currentNode) {

		if (cameFrom.containsKey(currentNode.getKey())) {
			p = reconstructPath(cameFrom, cameFrom.get(currentNode.getKey()));
			p.add(currentNode);

			return p;
		} else {
			p = new ArrayList<Node>();
			p.add(currentNode);
			return p;
		}
	}

}
