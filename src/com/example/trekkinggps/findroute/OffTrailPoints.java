package com.example.trekkinggps.findroute;


import org.osmdroid.util.GeoPoint;

/**
 * Created by Ewelina Swiderska on 2014-11-30.
 * Off Trail points are:
 * * current location point
 * * closest to current location point on trail
 * Off trail points are used in order to draw path in straight line to found route
 */
public class OffTrailPoints {

    private GeoPoint currentLocation;
    private GeoPoint closestPointOnTrail;

    public OffTrailPoints(GeoPoint currentLocation, GeoPoint closestPointOnTrail) {
        this.currentLocation = currentLocation;
        this.closestPointOnTrail = closestPointOnTrail;
    }

    public GeoPoint getClosestPointOnTrail() {
        return closestPointOnTrail;
    }

    public GeoPoint getCurrentLocation() {
        return currentLocation;
    }
}
