package com.example.trekkinggps.findroute;

import java.util.List;

import android.text.Html;
import com.example.trekkinggps.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * adapter dla listy dopasowan w trybie wyszukiwania
 */
public class SearchAdapter extends ArrayAdapter<String> {

	private final List<SearchLocationPoint> valuesLocationPoints;
	private Context context;
	private List<String> values;

	public SearchAdapter(Context context, List<String> values, List<SearchLocationPoint> valuesLocationPoints) {
		super(context, R.layout.search_itemview, values);
		this.context = context;
		this.values = values;
		this.valuesLocationPoints = valuesLocationPoints;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater
				.inflate(R.layout.search_itemview, parent, false);
		TextView textView = (TextView) rowView
				.findViewById(R.id.searchItemText);
		textView.setText(Html.fromHtml(values.get(position)));

		return rowView;
	}


	public SearchLocationPoint getLocationPoint(int position) {
		return valuesLocationPoints.get(position);
	}

}
