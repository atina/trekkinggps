package com.example.trekkinggps.findroute;

import org.osmdroid.util.GeoPoint;

/**
 * punkt posiadajacy opis lokalizacji wykorzystywany przy wyszukiwaniu
 * dopasowania lokalizacji
 */
public class SearchLocationPoint extends GeoPoint {

	private static final long serialVersionUID = 1L;

	private String location;
	private String description;


	public SearchLocationPoint(double aLatitude, double aLongitude) {
		super(aLatitude, aLongitude);
	}

	public SearchLocationPoint(String location, String description, double aLatitude, double aLongtitude) {
		super(aLatitude, aLongtitude);
		this.location = location;
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	public String getDescription() {
		return description;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
