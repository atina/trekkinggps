package com.example.trekkinggps.findroute;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.example.trekkinggps.astar.AStar;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.trekkinggps.R;
import com.example.trekkinggps.activities.FindRouteActivity;
import com.example.trekkinggps.activities.RoutesFoundActivity;
import com.example.trekkinggps.databases.RouteDatabaseHelper;
import com.example.trekkinggps.findroute.model.FoundRoute;
import com.example.trekkinggps.tiles.MyItemizedOverlay;

/**
 * klasa reagujaca na klikniecie na pozycje w menu activity odpowiedzialnym za
 * wyszukiwanie trasy
 */

public class PanelClickListener implements ListView.OnItemClickListener,
		TextWatcher, Button.OnClickListener, EditText.OnTouchListener {

	private final FindRouteActivity activity;
	MyItemizedOverlay myItemizedOverlay;
	private final MapView mapView;
	private final ListView panel;
	private final LinearLayout searchView;
	private final EditText searchText;
	private final ListView searchList;
	private final ImageButton panelHideButton;
	private final RouteDatabaseHelper database;
	boolean gpsEnabled = false;
	boolean isRoute = false;
	FoundRoute foundRoute = null;
	private ProgressDialog progressDialog;

	public PanelClickListener(FindRouteActivity activity,
			MyItemizedOverlay myItemizedOverlay, MapView mapView, ListView lview) {
		this.activity = activity;
		this.myItemizedOverlay = myItemizedOverlay;
		this.mapView = mapView;
		panel = lview;
		this.searchView = (LinearLayout) activity
				.findViewById(R.id.searchLayout);
		this.searchText = (EditText) searchView.findViewById(R.id.searchText);
		searchText.addTextChangedListener(this);
		searchText.setOnTouchListener(this);
		this.searchList = (ListView) activity.findViewById(R.id.searchList);
		searchList.setOnItemClickListener(this);
		panelHideButton = (ImageButton) activity.findViewById(R.id.hide_button);
		panelHideButton.setOnClickListener(this);
		this.database = new RouteDatabaseHelper(activity);

	}

	/**
	 * Metoda wywolujaca sie po kliknieciu na: 1)Jedna z propozycji na liscie
	 * w wyszukiwaniu (część if) wtedy ustawia tekst w okienku wyszukiwania
	 * na klikniety i zmienia kolor tekstu na przycisku na jasny i robi go
	 * "klikalnym" (mozna dodac jako punkt) 2)W else -> klikniecie na przycisk w
	 * panelu bocznym - dokonuje odpowiednich akcji w metodzie selectItem
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (parent.getId() == searchList.getId()) {
			SearchLocationPoint locationPoint = ((SearchAdapter) parent.getAdapter()).getLocationPoint(position);
			if (locationPoint != null) {
				myItemizedOverlay
						.addItem(locationPoint, "geopoint", "geopoint");
				mapView.invalidate();
				mapView.getController().animateTo(locationPoint);
			} else {
				FindRouteActivity.toast = Toast.makeText(activity,
						R.string.serach_add_point_fail, Toast.LENGTH_SHORT);
				FindRouteActivity.toast.show();
			}
			searchText.setText("");
			((InputMethodManager) activity
					.getSystemService(Activity.INPUT_METHOD_SERVICE))
					.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);

		} else {
			selectItem(position, view);
		}

	}

	/**
	 * metoda wykonujaca akcje po kliknieciu przycisku na panelu bocznym
	 * setPointsModes - wlacza/wylacza tryby dodawania punktow (pierwszy
	 * argument) i edycji punktow (drugi argument) setSearchVisibility -
	 * ukrywa/pokazuje panel wyszukiwania (ta szukajka na gorze)
	 */
	private void selectItem(int position, final View view) {
		if (position == 0) {
			// tryb wyszukiwania
			setPointsModes(false, false);
			setSearchVisibility(View.VISIBLE);
			activity.enableInteractivity();
		} else if (position == 1) {
			// tryb dodawania punktow
			setPointsModes(true, false);
			setSearchVisibility(View.GONE);
			activity.disableInteractivity();
		} else if (position == 2) {
			// tryb edycji punktow
			setPointsModes(false, true);
			setSearchVisibility(View.GONE);
			activity.disableInteractivity();
		} else if (position == 3) {
			// tryb opcji
			setPointsModes(false, false);
			setSearchVisibility(View.GONE);
			getSettingDialog().show();
			activity.disableInteractivity();
		} else if (position == 4) {
			// tryb lokalizacji jako punkt startowy
			setPointsModes(false, false);
			setSearchVisibility(View.GONE);
			activity.disableInteractivity();
			if (!gpsEnabled) {
				getAddGpsPointDialog().show();
			} else {
				getRemoveGpsPointDialog().show();
			}
		} else if (position == 5) {

			// tryb wyszukiwania trasy
			setPointsModes(false, false);
			setSearchVisibility(View.GONE);
			activity.disableInteractivity();

			final FindRoute findRoute = new FindRoute();

			AsyncTask<Void, Void, Boolean> searchRoutesTask = new AsyncTask<Void, Void, Boolean>() {

				@Override
				protected void onPreExecute() {
					progressDialog = new ProgressDialog(activity);
					progressDialog.setTitle(R.string.progressdialog_title);
					progressDialog.setMessage(activity.getResources().getString(R.string.progressdialog_message));
					progressDialog.setCancelable(false);
					progressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, activity.getResources().getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							AStar.cancelled = true;
						}
					});
					progressDialog.show();
				}

				@Override
				protected Boolean doInBackground(Void... arg0) {
					// wyszukuje trasy
					openDatabases();

					findRoute.setDatabase(database);
					boolean isRoute = findRoute.getRoute(myItemizedOverlay.getPoints());

					closeDatabases();

					return isRoute;
				}

				@Override
				protected void onPostExecute(Boolean result) {

					// jesli jest trasa to ja wyswietla w nowym activity
					if(!AStar.cancelled) {
						if (!result) {
							Toast.makeText(activity,
									"Cannot find route - no connection between trails!",
									Toast.LENGTH_LONG).show();
						} else {
							FoundRoute foundRoute = findRoute.getFoundRoute();
							Intent intent = new Intent(activity, RoutesFoundActivity.class);
							intent.putExtra("zoom", mapView.getZoomLevel());
							intent.putExtra("latitude", mapView.getMapCenter()
									.getLatitude());
							intent.putExtra("longtitude", mapView.getMapCenter()
									.getLongitude());
							intent.putExtra("foundroute", foundRoute);
							view.getContext().startActivity(intent);
						}
					}

					if (progressDialog != null) {
						progressDialog.dismiss();
					}
				}

			};

			searchRoutesTask.execute();

		}

	}

	private void closeDatabases() {
		database.close();
		database.closeSpatialite();
	}

	private void openDatabases() {
		database.openDataBase();
		database.openSpatialite();
	}

	// ustawia widocznosc panelu wyszukiwania i listy jego propozycji
	private void setSearchVisibility(int visibility) {
		searchView.setVisibility(visibility);
		searchList.setVisibility(visibility);
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	/**
	 * wywolywana kiedy tekst w panelu wyszkiwania sie zmienia
	 */
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// jesli dlugosc tekstu jest niepusta to wyszukuje w bazie
		if (s.length() > 2) {
			database.openDataBase();
			Cursor cursor = database.searchByInputText(s.toString());
			List<String> foundLocations = new ArrayList<String>();
			List<SearchLocationPoint> foundLocationsSearchPoints = new ArrayList<SearchLocationPoint>();
			if (cursor.moveToFirst()) {
				while (!cursor.isAfterLast()) {

					String location = cursor.getString(cursor.getColumnIndex("location"));
					String description = cursor.getString(cursor.getColumnIndex("description")).substring(2);
					double latitude = cursor.getDouble(cursor.getColumnIndex("lat"));
					double longitude = cursor.getDouble(cursor.getColumnIndex("lon"));

					String locationToShow = "<b>" + location + "</b><br/><i>" + description + "</i>";
					foundLocations.add(locationToShow);

					SearchLocationPoint searchLocationPoint = new SearchLocationPoint(location, description, latitude, longitude);
					foundLocationsSearchPoints.add(searchLocationPoint);

					cursor.moveToNext();
				}
			}

			// poniżej jest ograniczanie listy, do 4 elementow sie dopasowuje
			// do ilosci elementow, powyzej jest stalej dlugosci z suwakiem
			if (foundLocations.size() > 4) {
				RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) searchList
						.getLayoutParams();
				layoutParams.height = 200;
				searchList.setLayoutParams(layoutParams);

			} else {
				RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) searchList
						.getLayoutParams();
				layoutParams.height = android.view.WindowManager.LayoutParams.WRAP_CONTENT;
				searchList.setLayoutParams(layoutParams);
			}

			searchList.setAdapter(new SearchAdapter(activity, foundLocations, foundLocationsSearchPoints));
			database.close();
		} else {
			// jesli tekst jest pusty to ustawia spowrotem liste propozycji na
			// dopasowywanie sie do ilosci elementow (a że ich nie ma to bedzie
			// niewidoczna)
			RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) searchList
					.getLayoutParams();
			layoutParams.height = android.view.WindowManager.LayoutParams.WRAP_CONTENT;
			searchList.setLayoutParams(layoutParams);
			searchList.setAdapter(new SearchAdapter(activity,
					new ArrayList<String>(), null));
		}

	}

	/**
	 * ukrywa/pokazuje panel z opcjami po kliknieciu na przycisk
	 */
	@Override
	public void onClick(View arg0) {
		if (panel.getVisibility() == View.VISIBLE) {
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) panelHideButton.getLayoutParams();
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			panel.setVisibility(View.GONE);
			panelHideButton.setImageResource(R.drawable.ic_hide_left);
			panelHideButton.setLayoutParams(params);

			RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) searchView.getLayoutParams();
			params2.setMargins(0, 0, 50, 0);
			searchView.setLayoutParams(params2);

			RelativeLayout.LayoutParams params3 = (RelativeLayout.LayoutParams) searchList.getLayoutParams();
			params3.setMargins(0, 0, 0, 0);
			searchList.setLayoutParams(params3);
		} else {
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) panelHideButton.getLayoutParams();
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
			panel.setVisibility(View.VISIBLE);
			panelHideButton.setImageResource(R.drawable.ic_hide_right);
			panelHideButton.setLayoutParams(params);

			RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) searchView.getLayoutParams();
			params2.setMargins(0, 0, 120, 0);
			searchView.setLayoutParams(params2);

			RelativeLayout.LayoutParams params3 = (RelativeLayout.LayoutParams) searchList.getLayoutParams();
			params3.setMargins(0, 0, 70, 0);
			searchList.setLayoutParams(params3);
		}

	}

	/**
	 * metoda wylaczajaca klawiature i czyszcząca okienko wyszukiwania po
	 * kliknieciu na "X" w tym okienku
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_UP) {
			if (event.getRawX() >= (searchText.getRight() - searchText
					.getCompoundDrawables()[2].getBounds().width())) {
				searchText.setText("");
				((InputMethodManager) activity
						.getSystemService(Activity.INPUT_METHOD_SERVICE))
						.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
			}
		}
		return false;
	}

	// okienko opcji
	public AlertDialog getSettingDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		LayoutInflater inflater = activity.getLayoutInflater();

		builder.setTitle(R.string.setting_text);
		builder.setView(inflater.inflate(R.layout.settings_dialog, null));
		builder.setNeutralButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});

		return builder.create();
	}

	/**
	 * okienko dodawania lokalizacji jako punkt startowy
	 */
	public AlertDialog getAddGpsPointDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage("Do you want to add your current location as start point?");
		builder.setPositiveButton(R.string.button_yes,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						activity.followLocation();
						gpsEnabled = true;

					}
				}).setNegativeButton(R.string.button_cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});
		return builder.create();
	}

	/**
	 * okienko usuwania lokalizacji jako punktu startowego
	 */
	public AlertDialog getRemoveGpsPointDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage("Do you want to remove your current location as start point?");
		builder.setPositiveButton(R.string.button_yes,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						activity.disableLocation();
						myItemizedOverlay.removeItemFromFirstPosition();
						mapView.invalidate();
						gpsEnabled = false;
					}
				}).setNegativeButton(R.string.button_cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					}
				});
		return builder.create();
	}

	/**
	 * ustawienia trybow dla punktow (dodawanie/edycja)
	 */
	public void setPointsModes(boolean enableAddingPoint,
			boolean enableEditingPoints) {
		if (enableAddingPoint) {
			myItemizedOverlay.enableAddingPoints();
		} else {
			myItemizedOverlay.disableAddingPoints();
		}
		if (enableEditingPoints) {
			myItemizedOverlay.enableEditingPoints();
		} else {
			myItemizedOverlay.disableEditingPoints();
		}
	}
}
