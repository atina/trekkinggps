package com.example.trekkinggps.findroute.model;

import java.io.Serializable;

import com.example.trekkinggps.findroute.model.Node.Color;
import com.j256.ormlite.table.DatabaseTable;
import org.osmdroid.util.GeoPoint;

@DatabaseTable(tableName = "route_part")
public class RoutePart extends FoundRoute implements Serializable {

	private static final long serialVersionUID = 1L;

	private Color color;
	private double centerLat;
	private double centerLon;
	private double dividerLat;
	private double dividerLon;

	public void setCenterPoint(GeoPoint geoPoint) {
		this.centerLat = geoPoint.getLatitude();
		this.centerLon = geoPoint.getLongitude();
	}

	public GeoPoint getCenterPoint() {
		return new GeoPoint(centerLat, centerLon);
	}

	public void setDividerPoint(GeoPoint geoPoint) {
		this.dividerLat = geoPoint.getLatitude();
		this.dividerLon = geoPoint.getLongitude();
	}

	public GeoPoint getDividerPoint() {
		return new GeoPoint(dividerLat, dividerLon);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}
