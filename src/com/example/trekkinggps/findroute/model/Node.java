package com.example.trekkinggps.findroute.model;

import java.io.Serializable;

import com.example.trekkinggps.astar.Key;

public class Node implements Serializable {

	public enum Color {
		BLACK, BLUE, GREEN, RED, YELLOW, PURPLE, MULTI
	}

	private static final long serialVersionUID = 1L;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(lat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(lon);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (Double.doubleToLongBits(lat) != Double.doubleToLongBits(other.lat))
			return false;
		if (Double.doubleToLongBits(lon) != Double.doubleToLongBits(other.lon))
			return false;
		return true;
	}

	private Long id;

	private double lon;

	private double lat;

	private Color color;

	// Distance from source along optimal path
	private double g=-1;

	// Cost of move to this node;
	private double distance;

	private double f;

	// Heuristic estimate of distance from the current node to the target node
	private double h;

	private Long wayId;
	private int wayPos;

	private final Key key = new Key();

	public Long getId() {
		return id;
	}

	public void setId(Long i) {
		this.id = i;
		key.setId(i);
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getG() {
		return g;
	}

	public void setG(double g) {
		this.g = g;
	}

	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}

	public double getF() {
		return f;
	}

	public void setF(double f) {
		this.f = f;
	}

	public Long getWayId() {
		return wayId;
	}

	public void setWayId(Long wayId) {
		key.setWayId(wayId);
		this.wayId = wayId;
	}

	public int getWayPos() {
		return wayPos;
	}

	public void setWayPos(int wayPos) {
		this.wayPos = wayPos;
	}

	public Key getKey() {
		return key;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

}