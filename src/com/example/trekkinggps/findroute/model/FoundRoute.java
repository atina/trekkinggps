package com.example.trekkinggps.findroute.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.example.trekkinggps.findroute.DividerDrawer;
import com.example.trekkinggps.findroute.OffTrailPoints;

import com.example.trekkinggps.tiles.RouteDescriptionOverlay;
import org.osmdroid.util.GeoPoint;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequenceFactory;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequenceFactory;

/**
 * klasa przechowujaca dane o znalezionej trasie
 */

public class FoundRoute implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Node> fullPointsList = new ArrayList<Node>();

	private Geometry geometry = null;

	private Map<Integer, RoutePart> routeDesctiptionToDistance = new LinkedHashMap<Integer, RoutePart>();

	protected double routeLength = 0;

	private OffTrailPoints offTrailPoints = null;

	public List<Node> getFullPointsList() {
		return fullPointsList;
	}

	public double getRouteLength() {
		return routeLength;
	}

	public void setRouteLength(double routeLength) {
		this.routeLength = routeLength;
	}

	public void clearMap() {
		routeDesctiptionToDistance = new LinkedHashMap<Integer, RoutePart>();
	}

	public List<String> getRouteDescription(DividerDrawer dividerDrawer, RouteDescriptionOverlay routesDescriptionOverlay) {

		List<String> routeDescriptions = new ArrayList<String>();
		int i = 1;
		for (RoutePart part : routeDesctiptionToDistance.values()) {
			if (!part.getTime().equals(calculateTravelTime(0))) {
				dividerDrawer.addDivider(part.getDividerPoint());
				routesDescriptionOverlay.addDescription(part.getColor().toString(), part.getTime(), part.getCenterPoint());
				routeDescriptions.add(i + ". " + part.getColor() + "<br/><i>" + part.getTime() + "</i>");
				i++;
			}
		}
		return routeDescriptions;
	}

	private String calculateTravelTime(double length) {

		double lengthInKm = length / 1000;
		double timeInMinutes = lengthInKm * 15;
		int hours = (int) (Math.floor(timeInMinutes) / 60);
		int minutes = (int) (Math.floor(timeInMinutes) % 60);
		minutes = (int) (minutes % 10 > 5 ? Math.floor(minutes / 10) * 10 + 10 : Math.floor(minutes / 10) * 10);
		if (minutes >= 60) {
			minutes = 0;
			hours = 1;
		}
		String time = hours + "h " + minutes + "m ";

		return time;
	}

	public String getTime() {
		String time;
		if(this instanceof RoutePart) {
			time = calculateTravelTime(routeLength);
		} else {
			int hours = 0;
			int minutes = 0;
			for(RoutePart part : routeDesctiptionToDistance.values()) {
				hours += convertToHours(part.getTime());
				minutes += convertToMinutes(part.getTime());
			}
			hours += minutes / 60;
			minutes = minutes % 60;
			time =  hours + "h " + minutes + "m ";
		}

		return time;
	}

	private int convertToHours(String time) {
		return Integer.valueOf(time.split("h")[0]);
	}

	private int convertToMinutes(String time) {
		return Integer.valueOf(time.split("h ")[1].replace("m ",""));
	}

	public void addDistanceMap(int routePartNumber, RoutePart routePart) {
		routeDesctiptionToDistance.put(routePartNumber, routePart);
	}

	public RoutePart getDistanceMap(int routePartNumber) {
		return routeDesctiptionToDistance.get(routePartNumber);
	}

	public Collection<RoutePart> getRouteDesctiptionToDistance() {
		return routeDesctiptionToDistance.values();
	}

	public void setRouteDesctiptionToDistance(List<RoutePart> routeDistance) {
		for (int i = 0; i < routeDistance.size(); i++) {
			this.routeDesctiptionToDistance.put(i, routeDistance.get(i));
		}
	}

	public void addDistance(double distance) {
		this.routeLength += distance;
	}

	public double getDistance() {
		return this.routeLength;
	}

	public void setFullPointsList(List<Node> fullPointsList) {
		this.fullPointsList = fullPointsList;
	}

	public void calculateGeometry() {
		Coordinate[] coordinates = new Coordinate[fullPointsList.size()];
		for (int i = 0; i < fullPointsList.size(); i++) {
			coordinates[i] = new Coordinate(fullPointsList.get(i).getLat(), fullPointsList.get(i).getLon());
		}

		CoordinateSequenceFactory coordinateSequenceFactory = CoordinateArraySequenceFactory.instance();
		geometry = new GeometryFactory(coordinateSequenceFactory).createLineString(coordinates);
	}

	public boolean isPointOnRoute(GeoPoint geoPoint) {
		Coordinate coordinate = new Coordinate(geoPoint.getLatitude(), geoPoint.getLongitude());
		Geometry pointGeometry = new GeometryFactory().createPoint(coordinate);

		// distance is returned as angular distance units, so i used (PI/180) *
		// 6378137 to convert it to meters
		return geometry.distance(pointGeometry) * (Math.PI / 180) * 6378137 < 200;
	}

	public void calculateOffTrailPoints(GeoPoint geoPoint) {
		Coordinate locationCoordinate = new Coordinate(geoPoint.getLatitude(), geoPoint.getLongitude());
		Coordinate minCoordinate = new Coordinate();
		double distance = -1;
		for (Coordinate coordinate : geometry.getCoordinates()) {
			if (coordinate.distance(locationCoordinate) < distance || distance == -1) {
				distance = coordinate.distance(locationCoordinate);
				minCoordinate = coordinate;
			}
		}

		offTrailPoints = new OffTrailPoints(geoPoint, new GeoPoint(minCoordinate.x, minCoordinate.y));
	}

	public void clearOffTrailPoints() {
		this.offTrailPoints = null;
	}

	public OffTrailPoints getOffTrailPoints() {
		return this.offTrailPoints;
	}

	public boolean isOffTrail() {
		return (offTrailPoints != null);
	}

}
