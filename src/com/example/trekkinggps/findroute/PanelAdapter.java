package com.example.trekkinggps.findroute;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.example.trekkinggps.R;

/**
 * adapter dla listy przyciskow w panelu bocznym w activity odpowiedzialnym za
 * wyszukiwanie trasy
 */

public class PanelAdapter extends ArrayAdapter<String> {

	private final Context context;
	private final String[] values;

	public PanelAdapter(Context context, String[] values) {
		super(context, R.layout.items_layout, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.items_layout, parent, false);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);

		String s = values[position];
		if (s.equals("Find")) {
			imageView.setImageResource(R.drawable.ic_search);
		} else if (s.equals("Points")) {
			imageView.setImageResource(R.drawable.ic_points);
		} else if (s.equals("Options")) {
			imageView.setImageResource(R.drawable.ic_settings);
		} else if (s.equals("StartGps")) {
			imageView.setImageResource(R.drawable.ic_start_gps);
		} else if (s.equals("Ready")) {
			imageView.setImageResource(R.drawable.ic_ready);
		} else if (s.equals("EditPoints")) {
			imageView.setImageResource(R.drawable.ic_points_edit_blue);
		}

		return rowView;
	}

}
