package com.example.trekkinggps.findroute;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ewelina on 2015-01-04.
 */
public class DividerDrawer extends Overlay {

    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Path path = new Path();
    private List<GeoPoint> dividerNodes = new ArrayList<GeoPoint>();

    public DividerDrawer(Context ctx) {
        super(ctx);
    }


    public void addDivider(GeoPoint geoPoint) {
        this.dividerNodes.add(geoPoint);
    }

    private void initPaint() {
        paint.setDither(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(6);
    }



    @Override
    protected void draw(Canvas canvas, MapView mapView, boolean b) {
        initPaint();
        boolean first = true;
        for(int i = 0; i < dividerNodes.size() - 1; i++) {
            final android.graphics.Point p = new android.graphics.Point();
            MapView.Projection projection = mapView.getProjection();
            projection.toPixels(dividerNodes.get(i), p);
            if(first) {
                path.rewind();
            }
            first = false;
            path.moveTo(p.x, p.y);
            path.lineTo(p.x + 3, p.y + 3);
        }
        canvas.drawPath(path, paint);
    }
}
