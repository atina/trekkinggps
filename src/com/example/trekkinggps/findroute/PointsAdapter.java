package com.example.trekkinggps.findroute;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.trekkinggps.R;
import com.example.trekkinggps.tiles.MyItemizedOverlay;

/**
 * adapter dla listy punktow w liscie "na dole" - lista z punktami
 */
public class PointsAdapter extends ArrayAdapter<String> {

	private Context context;
	private List<String> values;
	ImageButton upButton;
	ImageButton downButton;
	ImageButton removeButton;
	private MyItemizedOverlay myItemizedOverlay;

	public PointsAdapter(Context context, List<String> values,
			MyItemizedOverlay myItemizedOverlay) {
		super(context, R.layout.pointlist_itemview, values);
		this.context = context;
		this.values = values;
		this.myItemizedOverlay = myItemizedOverlay;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.pointlist_itemview, parent,
				false);

		final TextView textView = (TextView) rowView
				.findViewById(R.id.pointItemText);
		textView.setText(Html.fromHtml(values.get(position)));

		upButton = (ImageButton) rowView.findViewById(R.id.up_button);
		downButton = (ImageButton) rowView.findViewById(R.id.down_button);
		removeButton = (ImageButton) rowView.findViewById(R.id.remove_button);

		// dodanie reakcji na klikniecie na przyciski up/down/remove w liscie z
		// punktami
		upButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				myItemizedOverlay.changePosition(position, -1);
			}
		});

		downButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				myItemizedOverlay.changePosition(position, +1);
			}
		});

		removeButton.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				myItemizedOverlay.remove(position);

			}
		});

		// jesli punkt na liscie jest pierwszy/ostatni to wylacza odpowiednio
		// przyciski up/down
		if (position == 0) {
			upButton.setVisibility(View.GONE);
		} else if (position == values.size() - 1) {
			downButton.setVisibility(View.GONE);
		}

		return rowView;
	}

}
