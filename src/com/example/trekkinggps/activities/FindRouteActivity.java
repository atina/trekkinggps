package com.example.trekkinggps.activities;

import java.io.File;

import android.content.Intent;
import android.view.MenuItem;
import com.example.trekkinggps.commons.LocationUpdatesConstants;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.tileprovider.MapTileProviderArray;
import org.osmdroid.tileprovider.modules.IArchiveFile;
import org.osmdroid.tileprovider.modules.MBTilesFileArchive;
import org.osmdroid.tileprovider.modules.MapTileFileArchiveProvider;
import org.osmdroid.tileprovider.modules.MapTileModuleProviderBase;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import android.app.Activity;
import android.content.Context;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.widget.ListView;
import android.widget.Toast;

import com.example.trekkinggps.R;
import com.example.trekkinggps.databases.WaysDatabaseHelper;
import com.example.trekkinggps.findroute.PanelAdapter;
import com.example.trekkinggps.findroute.PanelClickListener;
import com.example.trekkinggps.tiles.MainTilesOverlay;
import com.example.trekkinggps.tiles.MyItemizedOverlay;

public class FindRouteActivity extends Activity {

	public static Toast toast;
	private MyLocationNewOverlay myLocationOverlay;
	private boolean gpsEnabled = false;
	MapView mapView;
	MainTilesOverlay tilesOverlay2;
	MyItemizedOverlay myItemizedOverlay;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.find_route);
		int zoomLevel = getIntent().getExtras().getInt("zoom");
		IGeoPoint geoPoint = new GeoPoint(getIntent().getExtras().getDouble(
				"latitude"), getIntent().getExtras().getDouble("longtitude"));
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);

		mapView.getController().setCenter(geoPoint);
		mapView.getController().setZoom(zoomLevel);

		mapView.getController().animateTo(geoPoint);

		String path = Environment.getExternalStorageDirectory() + "/";
		// Layer for 6-12 zoom
		File file = new File(path, "trekkingBicycle14.mbtiles");

		// Layer for 12-14 zoom
		// File file2 = new File(path, "TrekkingGPS6-12.mbtiles");

		IArchiveFile[] files = { MBTilesFileArchive
				.getDatabaseFileArchive(file) };

		MapTileModuleProviderBase moduleProvider;
		SimpleRegisterReceiver sr = new SimpleRegisterReceiver(this);
		XYTileSource tSource;
		tSource = new XYTileSource("mbtiles",
				ResourceProxy.string.offline_mode, 6, 17, 256, ".png",
				new String[] { "" });
		moduleProvider = new MapTileFileArchiveProvider(sr, tSource, files);

		MapTileModuleProviderBase[] pBaseArray;
		pBaseArray = new MapTileModuleProviderBase[] { moduleProvider };

		MapTileProviderArray provider;
		provider = new MapTileProviderArray(tSource, null, pBaseArray);

		WaysDatabaseHelper myDbHelper = new WaysDatabaseHelper(this);

		try {
			myDbHelper.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}

		tilesOverlay2 = new MainTilesOverlay(provider, this.getBaseContext(),
				myDbHelper);
		tilesOverlay2.setInteractivityMode(false);
		tilesOverlay2.setLoadingBackgroundColor(Color.TRANSPARENT);

		mapView.getOverlays().add(tilesOverlay2);

		myDbHelper.close();

		// inicjacja warstwy na punkty
		Drawable marker = getResources().getDrawable(R.drawable.ic_point_blue);
		int markerWidth = marker.getIntrinsicWidth();
		int markerHeight = marker.getIntrinsicHeight();
		marker.setBounds(-markerWidth / 2, -markerHeight, markerWidth / 2, 0);

		ResourceProxy resourceProxy = new DefaultResourceProxyImpl(
				getApplicationContext());
		ListView listView = (ListView) findViewById(R.id.left_drawer);
		myItemizedOverlay = new MyItemizedOverlay(marker, resourceProxy, this,
				listView, mapView);
		mapView.getOverlays().add(myItemizedOverlay);

		// tworzenie View dla bocznego panelu
		String[] options = getResources().getStringArray(R.array.options);
		ListView lview = (ListView) findViewById(R.id.right_drawer);

		// PanelAdapter - tu ustawiane s� elementy bocznego panelu
		lview.setAdapter(new PanelAdapter(this, options));
		// PanelClickListener - tu jest okreslane co ma robic po kliknieciu na
		// przycisk w bocznym panelu
		lview.setOnItemClickListener(new PanelClickListener(this,
				myItemizedOverlay, mapView, lview));

		super.onCreate(savedInstanceState);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.getItem(0).setVisible(false);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.import_route:
				Intent intent = new Intent(this, ManageRoutesActivity.class);
				intent.putExtra("zoom", mapView.getZoomLevel());
				intent.putExtra("latitude", mapView.getMapCenter().getLatitude());
				intent.putExtra("longtitude", mapView.getMapCenter().getLongitude());
				mapView.getContext().startActivity(intent);
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * wywolywane jesli chcemy usunac biezaca lokalizacje z punktu startowego
	 */
	public void disableLocation() {
		gpsEnabled = false;
	}

	/**
	 * funkcje w��czaj�ce/wy��czaj�ce interaktywno��
	 */
	public void disableInteractivity() {
		tilesOverlay2.setInteractivityMode(false);
	}

	public void enableInteractivity() {
		tilesOverlay2.setInteractivityMode(true);
	}

	/**
	 * rozpoczyna sledzenie lokalizacji jesli kliknie sie na "dodaj biezaca
	 * lokalizacje jako punkt startowy)
	 */
	public void followLocation() {
		gpsEnabled = true;
		myLocationOverlay = new MyLocationNewOverlay(this, mapView);

		mapView.getOverlays().add(myLocationOverlay);

		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, LocationUpdatesConstants.MINIMUM_TIME, LocationUpdatesConstants.MINIMUM_DISTANCE,
				locationListener);
	}

	/**
	 * po zmianie lokalizacji update'uje punkt na mapie i jego wspolrzedne w
	 * liscie (jesli wlaczona opcja lokalizacja jako punkt startowy)
	 */
	protected void updateLocation(Location location) {
		if (gpsEnabled) {
			GeoPoint geoPoint = new GeoPoint(location.getLatitude(),
					location.getLongitude());
			myItemizedOverlay.addItemAtPosition(geoPoint, 0, true);
			mapView.invalidate();
			mapView.getController().animateTo(geoPoint);

		}
	}

	/**
	 * listener kt�ry reaguje na zmiany lokalizacji i w przypadku jej wykrycia
	 * od�wie�a bie��c� lokalizacj�
	 */
	private final LocationListener locationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLocationChanged(Location location) {
			updateLocation(location);

		}
	};

	@Override
	protected void onPause() {
		if (null != FindRouteActivity.toast) {
			FindRouteActivity.toast.cancel();
		}

		super.onPause();
	};

	@Override
	protected void onDestroy() {
		if (null != FindRouteActivity.toast) {
			FindRouteActivity.toast.cancel();
		}
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		if (null != FindRouteActivity.toast) {
			FindRouteActivity.toast.cancel();
		}
		super.onStop();
	}

}