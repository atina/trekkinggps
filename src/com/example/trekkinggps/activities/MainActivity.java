package com.example.trekkinggps.activities;

import java.io.File;

import com.example.trekkinggps.commons.LocationUpdatesConstants;
import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.IRegisterReceiver;
import org.osmdroid.tileprovider.MapTileProviderArray;
import org.osmdroid.tileprovider.modules.IArchiveFile;
import org.osmdroid.tileprovider.modules.MBTilesFileArchive;
import org.osmdroid.tileprovider.modules.MapTileFileArchiveProvider;
import org.osmdroid.tileprovider.modules.MapTileModuleProviderBase;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.trekkinggps.R;
import com.example.trekkinggps.databases.WaysDatabaseHelper;
import com.example.trekkinggps.tiles.MainTilesOverlay;

public class MainActivity extends Activity implements IRegisterReceiver {

	public static Toast toast;
	private MyLocationNewOverlay myLocationOverlay = null;
	private MapView mapView;
	private boolean gpsEnabled;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		MapTileProviderArray provider = readTrailsTiles();

		WaysDatabaseHelper myDbHelper = new WaysDatabaseHelper(this);

		try {
			myDbHelper.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}

		MainTilesOverlay tilesOverlay2 = new MainTilesOverlay(provider, this.getBaseContext(), myDbHelper);
		tilesOverlay2.setInteractivityMode(true);
		tilesOverlay2.setLoadingBackgroundColor(Color.TRANSPARENT);

		initMapView(tilesOverlay2);

		myDbHelper.close();

		enableGpsDialog().show();

	}

	private void initMapView(MainTilesOverlay tilesOverlay2) {
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		mapView.getOverlays().add(tilesOverlay2);

		mapView.setBuiltInZoomControls(true);
		mapView.setMaxZoomLevel(17);
		mapView.getController().setZoom(6);
		mapView.getController().animateTo(new GeoPoint(50.03, 19.56));

	}

	private MapTileProviderArray readTrailsTiles() {
		String path = Environment.getExternalStorageDirectory() + "/";

		// Layer for 6-12 zoom
		File file = new File(path, "trekkingBicycle14.mbtiles");

		// Layer for 12-14 zoom
		// File file2 = new File(path, "TrekkingGPS6-12.mbtiles");

		IArchiveFile[] files = { MBTilesFileArchive.getDatabaseFileArchive(file) };

		MapTileModuleProviderBase moduleProvider;
		SimpleRegisterReceiver sr = new SimpleRegisterReceiver(this);
		XYTileSource tSource;
		tSource = new XYTileSource("mbtiles", ResourceProxy.string.offline_mode, 6, 17, 256, ".png",
				new String[] { "" });
		moduleProvider = new MapTileFileArchiveProvider(sr, tSource, files);

		MapTileModuleProviderBase[] pBaseArray;
		pBaseArray = new MapTileModuleProviderBase[] { moduleProvider };

		MapTileProviderArray provider;
		provider = new MapTileProviderArray(tSource, null, pBaseArray);
		return provider;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (gpsEnabled) {
			menu.getItem(0).setIcon(R.drawable.ic_location_blue);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.find_route:
			Intent intent = new Intent(this, FindRouteActivity.class);
			intent.putExtra("zoom", mapView.getZoomLevel());
			intent.putExtra("latitude", mapView.getMapCenter().getLatitude());
			intent.putExtra("longtitude", mapView.getMapCenter().getLongitude());
			mapView.getContext().startActivity(intent);
			return true;
		case R.id.location:
			if (gpsEnabled) {
				item.setIcon(R.drawable.ic_location_white);
				gpsEnabled = false;
			} else {
				item.setIcon(R.drawable.ic_location_blue);
				gpsEnabled = true;
				followLocation();
			}
			return true;
		case R.id.import_route:
			intent = new Intent(this, ManageRoutesActivity.class);
			intent.putExtra("zoom", mapView.getZoomLevel());
			intent.putExtra("latitude", mapView.getMapCenter().getLatitude());
			intent.putExtra("longtitude", mapView.getMapCenter().getLongitude());
			mapView.getContext().startActivity(intent);
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (gpsEnabled) {
			myLocationOverlay.enableMyLocation();
			myLocationOverlay.enableFollowLocation();
		}
	}

	@Override
	protected void onPause() {
		if (null != MainActivity.toast) {
			MainActivity.toast.cancel();
		}
		super.onPause();
		if (gpsEnabled) {
			myLocationOverlay.disableMyLocation();
			myLocationOverlay.disableFollowLocation();
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		if (gpsEnabled) {
			myLocationOverlay.enableMyLocation();
			myLocationOverlay.enableFollowLocation();
		}
	}

	protected Dialog enableGpsDialog() {
		AlertDialog.Builder dialog = new AlertDialog.Builder(this).setMessage(R.string.gps_question)
				.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						gpsEnabled = true;
						// invalidateOptionsMenu();
						followLocation();
					}
				}).setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						gpsEnabled = false;
					}
				});

		return dialog.create();
	}

	private void followLocation() {

		myLocationOverlay = new MyLocationNewOverlay(this, mapView);
		myLocationOverlay.runOnFirstFix(new Runnable() {

			@Override
			public void run() {
				mapView.getController().animateTo(myLocationOverlay.getMyLocation());
			}
		});

		myLocationOverlay.enableMyLocation();
		myLocationOverlay.enableFollowLocation();
		mapView.getOverlays().add(myLocationOverlay);

		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, LocationUpdatesConstants.MINIMUM_TIME, LocationUpdatesConstants.MINIMUM_DISTANCE, locationListener);
	}

	protected void updateLocation(Location location) {
		if (gpsEnabled) {
			GeoPoint geoPoint = new GeoPoint(location.getLatitude(), location.getLongitude());
			mapView.getController().animateTo(geoPoint);
		}
	}

	@Override
	protected void onDestroy() {
		if (null != MainActivity.toast) {
			MainActivity.toast.cancel();
		}
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		if (null != MainActivity.toast) {
			MainActivity.toast.cancel();
		}
		super.onStop();
	}

	private final LocationListener locationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLocationChanged(Location location) {
			updateLocation(location);

		}

	};

}