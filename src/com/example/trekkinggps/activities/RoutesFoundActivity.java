package com.example.trekkinggps.activities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import com.example.trekkinggps.findroute.DividerDrawer;
import com.example.trekkinggps.tiles.RouteDescriptionOverlay;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.tileprovider.MapTileProviderArray;
import org.osmdroid.tileprovider.modules.IArchiveFile;
import org.osmdroid.tileprovider.modules.MBTilesFileArchive;
import org.osmdroid.tileprovider.modules.MapTileFileArchiveProvider;
import org.osmdroid.tileprovider.modules.MapTileModuleProviderBase;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.TilesOverlay;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.widget.ListView;

import com.example.trekkinggps.R;
import com.example.trekkinggps.routesfound.FoundRouteClickListener;
import com.example.trekkinggps.routesfound.FoundRoutesAdapter;
import com.example.trekkinggps.routesfound.RoutesDescriptionAdapter;
import com.example.trekkinggps.findroute.FoundRouteDrawer;
import com.example.trekkinggps.findroute.model.FoundRoute;

public class RoutesFoundActivity extends Activity {

	private MapView mapView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.found_routes);

		int zoomLevel = getIntent().getExtras().getInt("zoom");
		IGeoPoint geoPoint = new GeoPoint(getIntent().getExtras().getDouble(
				"latitude"), getIntent().getExtras().getDouble("longtitude"));

		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		

		mapView.getController().setCenter(geoPoint);
		mapView.getController().setZoom(zoomLevel);

		String path = Environment.getExternalStorageDirectory() + "/";
		// Layer for 6-12 zoom
		File file = new File(path, "trekkingBicycle14.mbtiles");

		// Layer for 12-14 zoom
		// File file2 = new File(path, "TrekkingGPS6-12.mbtiles");

		IArchiveFile[] files = { MBTilesFileArchive
				.getDatabaseFileArchive(file) };

		MapTileModuleProviderBase moduleProvider;
		SimpleRegisterReceiver sr = new SimpleRegisterReceiver(this);
		XYTileSource tSource;
		tSource = new XYTileSource("mbtiles",
				ResourceProxy.string.offline_mode, 6, 17, 256, ".png",
				new String[] { "" });
		moduleProvider = new MapTileFileArchiveProvider(sr, tSource, files);

		MapTileModuleProviderBase[] pBaseArray;
		pBaseArray = new MapTileModuleProviderBase[] { moduleProvider };

		MapTileProviderArray provider;
		provider = new MapTileProviderArray(tSource, null, pBaseArray);

		final TilesOverlay tilesOverlay2 = new TilesOverlay(provider, this);
		tilesOverlay2.setLoadingBackgroundColor(Color.TRANSPARENT);
		mapView.getOverlays().add(tilesOverlay2);

		// inicjacja warstwy na opisy
		Drawable marker = getResources().getDrawable(R.drawable.ic_circle_black);
		ResourceProxy resourceProxy = new DefaultResourceProxyImpl(
				getApplicationContext());
		RouteDescriptionOverlay routesDescriptionOverlay = new RouteDescriptionOverlay(marker, resourceProxy, this);



		FoundRoute foundRoute = (FoundRoute) getIntent().getSerializableExtra(
				"foundroute");
		foundRoute.calculateGeometry();

		FoundRouteDrawer drawer = new FoundRouteDrawer(getBaseContext());
		drawer.setFoundRoute(foundRoute);
		mapView.getOverlays().add(drawer);
		mapView.getController().animateTo(geoPoint);
		String[] icons = { "Description", "Route", "Download", "StartTrail" };
		ListView lview = (ListView) findViewById(R.id.left_drawer);

		String[] headers = getResources().getStringArray(
				R.array.foundroute_headers);

		DividerDrawer dividerDrawer = new DividerDrawer(getBaseContext());


		List<String> data = new ArrayList<String>();
		data.add(headers[0]);
		data.addAll(1, foundRoute.getRouteDescription(dividerDrawer, routesDescriptionOverlay));
		data.add(headers[1]);
		data.add(foundRoute.getTime());

		mapView.getOverlays().add(dividerDrawer);
		mapView.getOverlays().add(routesDescriptionOverlay);

		ListView lvView2 = (ListView) findViewById(R.id.routes_description);
		lvView2.setAdapter(new RoutesDescriptionAdapter(this, data));

		// FoundRoutesAdapter - tu ustawiane są elementy bocznego panelu
		lview.setAdapter(new FoundRoutesAdapter(this, icons));
		// FoundRoutesClickListener - tu jest okreslane co ma robic po
		// kliknieciu na przycisk w bocznym panelu
		lview.setOnItemClickListener(new FoundRouteClickListener(this, mapView,
				foundRoute));

		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.getItem(0).setVisible(false);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.import_route:
				Intent intent = new Intent(this, ManageRoutesActivity.class);
				intent.putExtra("zoom", mapView.getZoomLevel());
				intent.putExtra("latitude", mapView.getMapCenter().getLatitude());
				intent.putExtra("longtitude", mapView.getMapCenter().getLongitude());
				mapView.getContext().startActivity(intent);
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
