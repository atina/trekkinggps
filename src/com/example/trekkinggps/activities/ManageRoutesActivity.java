package com.example.trekkinggps.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.trekkinggps.R;
import com.example.trekkinggps.databases.model.FoundRouteDb;
import com.example.trekkinggps.databases.service.FoundRouteService;
import com.example.trekkinggps.manage.routes.RoutesAdapter;

public class ManageRoutesActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.manage_routes);
		FoundRouteService service = new FoundRouteService(this);
		ListView listView = (ListView) findViewById(R.id.routes_list);
		listView.setAdapter(new RoutesAdapter(this, getSavedRoutes(service),
				service));

		super.onCreate(savedInstanceState);
	}

	private List<String> getSavedRoutes(FoundRouteService service) {

		List<FoundRouteDb> routes;
		final List<String> routesNames = new ArrayList<String>();
		try {
			routes = service.getAllFromDb();
			for (FoundRouteDb route : routes) {
				routesNames.add(route.toString());
			}

		} catch (java.sql.SQLException e) {
			Log.d("TrekkingGPS", "error during extracting routes from db");
		}
		return routesNames;
	}

}
