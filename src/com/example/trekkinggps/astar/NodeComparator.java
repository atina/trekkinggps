package com.example.trekkinggps.astar;

import java.util.Comparator;

import com.example.trekkinggps.findroute.model.Node;


public class NodeComparator implements Comparator<Node> {

	@Override
	public int compare(Node first, Node second) {
		if (first.getF() < second.getF()) {
			return -1;
		} else if (first.getF() > second.getF()) {
			return 1;
		} else {
			return 0;
		}
	}
}
