package com.example.trekkinggps.astar;

import java.io.Serializable;

public class Key implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Long wayId;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((getWayId() == null) ? 0 : getWayId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Key other = (Key) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (getWayId() == null) {
			if (other.getWayId() != null)
				return false;
		} else if (!getWayId().equals(other.getWayId()))
			return false;
		return true;
	}

	public Long getWayId() {
		return wayId;
	}

	public void setWayId(Long wayId) {
		this.wayId = wayId;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
