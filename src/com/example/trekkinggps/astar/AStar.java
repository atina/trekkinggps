package com.example.trekkinggps.astar;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import org.osmdroid.util.GeoPoint;

import android.location.Location;
import android.util.Log;

import com.example.trekkinggps.databases.RouteDatabaseHelper;
import com.example.trekkinggps.findroute.model.Node;

public class AStar {

	public static boolean cancelled;


	private static float calculateDistance(Node a, Node b) {
		GeoPoint p1 = new GeoPoint(a.getLat(), a.getLon());
		GeoPoint p2 = new GeoPoint(b.getLat(), b.getLon());
		float[] results = new float[3];
		Location.distanceBetween(p1.getLatitudeE6()/1E6, p1.getLongitudeE6()/1E6, p2.getLatitudeE6()/1E6, p2.getLongitudeE6()/1E6, results);
		return results[0];
	}

	public static Map<Key, Node> cameFrom = new HashMap<Key, Node>();

	public static boolean Astar(Node start, Node goal, RouteDatabaseHelper database) {

		cancelled = false;

		Log.d("TrekkingGPS", "szukam trasy dla: " + start.getId() + " " + start.getWayId() + " do: " + goal.getId()
				+ " " + goal.getWayId());


		// Zbior wierzcholkow przejrzanych.
		Map<Key,Node> closedset = new HashMap<Key,Node>();
		Map<Key, Node> openSet = new HashMap<Key, Node>();
		// Zbior wierzcholkow nie odwiedzonych.
		PriorityQueue<Node> openset = new PriorityQueue<Node>(20, new NodeComparator());
		openset.add(start);
		openSet.put(start.getKey(), start);
		cameFrom.put(start.getKey(), start);

		start.setG(0);
		start.setF(start.getG() + calculateDistance(start, goal));

		while (!cancelled && openset.size() > 0) {

			// wezel na gorze ma najmniejsza wartosc f
			Node current = openset.poll();

			openSet.remove(current.getKey());
			if (current.getKey().equals(goal.getKey())) {
				goal = current;
				return true;
			}

			openset.remove(openSet.get(current.getKey()));
			openSet.remove(current.getKey());
			closedset.put(current.getKey(), current);

			List<Node> neighbors = database.getNeighbors(current);
			for (Node neighbor : neighbors) {

				double cost = current.getG() + neighbor.getDistance();

				// sprawdzamy, czy nowa sciezka jest lepsza
//				if (openSet.containsKey(neighbor.getKey()) && cost < openSet.get(neighbor.getKey()).getG()) {
//					openset.remove(openSet.get(neighbor.getKey()));
//					openSet.remove(neighbor.getKey());
//				}

				// raczej nigdy nie powinno zajsc
				if (closedset.containsKey(neighbor.getKey()) && cost < closedset.get(neighbor.getKey()).getG())
					closedset.remove(neighbor.getKey());
				if (!openSet.containsKey(neighbor.getKey()) && !closedset.containsKey(neighbor.getKey())) {
					neighbor.setG(cost);
					neighbor.setH(calculateDistance(neighbor, goal));
					neighbor.setF(neighbor.getG() + neighbor.getH());
					openset.add(neighbor);
					openSet.put(neighbor.getKey(), neighbor);
					cameFrom.put(neighbor.getKey(), current);
				}
			}
			
		}
		return false;
	}

}
