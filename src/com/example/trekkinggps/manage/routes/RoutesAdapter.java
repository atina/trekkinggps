package com.example.trekkinggps.manage.routes;

import java.sql.SQLException;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.trekkinggps.R;
import com.example.trekkinggps.activities.RoutesFoundActivity;
import com.example.trekkinggps.databases.model.FoundRouteDb;
import com.example.trekkinggps.databases.service.FoundRouteService;
import com.example.trekkinggps.databases.translators.FoundRouteTranslator;
import com.example.trekkinggps.findroute.model.FoundRoute;

public class RoutesAdapter extends ArrayAdapter<String> {
	private Context context;
	private FoundRouteService service;
	private List<String> values;

	private ImageButton removeButton;
	private ImageButton editButton;
	private ImageButton startButton;

	private ArrayAdapter<String> thisAdapter =this;
	
	public RoutesAdapter(Context context, List<String> values,
			FoundRouteService service) {
		super(context, R.layout.routes_list, values);
		this.context = context;
		this.values = values;
		this.service = service;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.routes_list, parent, false);
		Log.d("mee", "lview");
		final TextView textView = (TextView) rowView
				.findViewById(R.id.savedRouteItemText);
		textView.setText(values.get(position));

		removeButton = (ImageButton) rowView.findViewById(R.id.remove_button);
		editButton = (ImageButton) rowView.findViewById(R.id.edit_button);
		startButton = (ImageButton) rowView
				.findViewById(R.id.start_trail_button);

		removeButton.setOnClickListener(createRemoveListener(position));

		editButton.setOnClickListener(createEditListener(position));

		startButton.setOnClickListener(createStartListener(position));

		return rowView;
	}

	private OnClickListener createStartListener(final int position) {
		return new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				goToFoundRouteActivity(position);
			}
		};
	}

	private OnClickListener createEditListener(final int position) {
		return new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				Builder alertBuilder = createPromptForName(values.get(position));
				alertBuilder.show();
			
			}
		};
	}

	private OnClickListener createRemoveListener(final int position) {
		return new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				Builder alertBuilder = createPromptForDelete(position);
				alertBuilder.show();			
			}
		};
	}

	private AlertDialog.Builder createPromptForDelete(final int position) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);

		alert.setTitle("Delete route");
		alert.setMessage("Do you really want to delete?");

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
				try {
					service.delete(values.get(position));
					values.remove(position);
					thisAdapter.notifyDataSetChanged();
				} catch (SQLException e) {
					Log.d("TrekkingGPS",
							"Error during delete " + e.getMessage());
				}
			}

		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});
		
		return alert;
	}

	private void goToFoundRouteActivity(int position) {
		try {

			FoundRouteTranslator serviceTranslator = new FoundRouteTranslator();
			FoundRouteDb foundRouteDb = service.getFromDb(values.get(position));
			FoundRoute foundRoute = serviceTranslator
					.translateFromDb(foundRouteDb);
			Intent intent = createFoundRouteActivity(foundRoute);
			context.startActivity(intent);

		} catch (java.sql.SQLException e) {
			Log.d("TrekkingGPS",
					"error during extracting route from db " + e.getMessage());
		}
	}

	private Intent createFoundRouteActivity(FoundRoute foundRoute) {
		Intent intent = new Intent(context, RoutesFoundActivity.class);

		intent.putExtra("foundroute", foundRoute);
		intent.putExtra("zoom", 10);
		intent.putExtra("latitude", foundRoute.getFullPointsList().get(0)
				.getLat());
		intent.putExtra("longtitude", foundRoute.getFullPointsList().get(0)
				.getLon());
		return intent;
	}

	private AlertDialog.Builder createPromptForName(final String oldName) {
		AlertDialog.Builder alert = new AlertDialog.Builder(context);

		alert.setTitle("Rename route");
		alert.setMessage("Enter new name for route");

		// Set an EditText view to get user input
		final EditText input = new EditText(context);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
				updateRoute(input, oldName);
				values.remove(oldName);
				values.add(input.getText().toString());
			}

		});

		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
					}
				});
		return alert;
	}

	private void updateRoute(EditText input, String oldName) {
		try {
			service.update(oldName, input.getText().toString());
			this.notifyDataSetChanged();
		} catch (SQLException e) {
			Log.d("TrekkingGPS", "Error during update " + e.getMessage());
		}

	}
}
