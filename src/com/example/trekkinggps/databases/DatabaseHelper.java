package com.example.trekkinggps.databases;

import java.io.File;

import jsqlite.Database;
import jsqlite.Exception;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

/**
 * Podstawowa klasa odpowiedzialna za podstawowe operacje zwiazane z baza danych
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	private static String DB_PATH = Environment.getExternalStorageDirectory() + "/";

	private static String DB_NAME = "mergedWithMultiAndPurple.osm.db";
	protected static String MAIN_TABLE_NAME = "mergedWithMultiAndPurple_polylines";

	protected SQLiteDatabase myDataBase;
	protected Database db;

	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, 1);
	}

	public void openDataBase() throws SQLException {

		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

	}

	public void openSpatialite() {

		File spatialDbFile = new File(DB_PATH, DB_NAME);

		db = new jsqlite.Database();
		try {
			db.open(spatialDbFile.getAbsolutePath(), jsqlite.Constants.SQLITE_OPEN_READWRITE | jsqlite.Constants.SQLITE_OPEN_CREATE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public synchronized void close() {

		if (myDataBase != null)
			myDataBase.close();

		super.close();

	}

	public void closeSpatialite() {
		if (db != null)
			try {
				db.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
