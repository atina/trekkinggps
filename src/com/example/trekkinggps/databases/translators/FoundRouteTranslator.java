package com.example.trekkinggps.databases.translators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.example.trekkinggps.databases.model.FoundRouteDb;
import com.example.trekkinggps.databases.model.NodeDb;
import com.example.trekkinggps.findroute.model.FoundRoute;
import com.example.trekkinggps.findroute.model.Node;
import com.example.trekkinggps.findroute.model.RoutePart;
import com.example.trekkinggps.findroute.model.Node.Color;
import org.osmdroid.util.GeoPoint;

public class FoundRouteTranslator {
	public FoundRouteDb translate(FoundRoute foundRoute) {
		FoundRouteDb foundRouteDb  = new FoundRouteDb();
		Collection<NodeDb> nodes = traslateNodes(
				foundRoute.getFullPointsList(), foundRouteDb);
		foundRouteDb.setFullPointsList(nodes);

		Collection<FoundRouteDb> routePartsDb = translateRouteParts(foundRoute
				.getRouteDesctiptionToDistance(), foundRouteDb);
		
		foundRouteDb.setRouteDesctiptionToDistance(routePartsDb);
		foundRouteDb.setLength(foundRoute.getRouteLength());
		return foundRouteDb;
	}

	private Collection<FoundRouteDb> translateRouteParts(
			Collection<RoutePart> routeDesctiptionToDistance, FoundRouteDb foundRouteDb) {
		Collection<FoundRouteDb> routePartsDb = new ArrayList<FoundRouteDb>();
		for (RoutePart routePart : routeDesctiptionToDistance) {
			FoundRouteDb routePartDb =  translate(routePart);
			routePartDb.setFoundRouteDb(foundRouteDb);
			String color=null;
			if(routePart.getColor()!=null)
				color=routePart.getColor().toString();
			if(routePart.getCenterPoint() != null) {
				routePartDb.setCenterLat(routePart.getCenterPoint().getLatitude());
				routePartDb.setCenterLon(routePart.getCenterPoint().getLongitude());
			}
			if(routePart.getDividerPoint() != null) {
				routePartDb.setDividerLat(routePart.getDividerPoint().getLatitude());
				routePartDb.setDividerLon(routePart.getDividerPoint().getLongitude());
			}
			routePartDb.setColor(color);
			routePartsDb.add(routePartDb);
		}
		return routePartsDb;
	}

	private Collection<NodeDb> traslateNodes(List<Node> fullPointsList,
			FoundRouteDb foundRouteDb) {
		Collection<NodeDb> nodes = new ArrayList<NodeDb>();
		for (Node node : fullPointsList) {
			NodeDb nodeDb = new NodeDb();
			nodeDb.setId(node.getId());
			nodeDb.setFoundRouteDb(foundRouteDb);
			nodeDb.setLat(node.getLat());
			nodeDb.setLon(node.getLon());
			nodes.add(nodeDb);
		}
		return nodes;
	}

	public FoundRoute translateFromDb(FoundRouteDb foundRouteDb) {
		FoundRoute foundRoute = null;
		if(foundRouteDb.getColor()==null)foundRoute=new FoundRoute();
		else foundRoute = new RoutePart();
		
		List<Node> nodes = traslateNodesFromDb(foundRouteDb.getFullPointsList());
		foundRoute.setFullPointsList(nodes);
	
		List<RoutePart> routeParts = translateRoutePartsFromDb(foundRouteDb
				.getRouteDesctiptionToDistance());
		foundRoute.setRouteDesctiptionToDistance(routeParts);
		foundRoute.setRouteLength(foundRouteDb.getLength());
		return foundRoute;
	}

	private List<RoutePart> translateRoutePartsFromDb(
			Collection<FoundRouteDb> routeDesctiptionToDistance) {
		List<RoutePart> routeParts = new ArrayList<RoutePart>();
		for (FoundRouteDb routePartDb : routeDesctiptionToDistance) {
			RoutePart routePart = (RoutePart) translateFromDb(routePartDb);
			routePart.setColor(Color.valueOf(routePartDb.getColor()));
			routePart.setCenterPoint(new GeoPoint(routePartDb.getCenterLat(), routePartDb.getCenterLon()));
			routePart.setDividerPoint(new GeoPoint(routePartDb.getDividerLat(), routePartDb.getDividerLon()));
			routeParts.add(routePart);
		}
		return routeParts;
	}

	private List<Node> traslateNodesFromDb(Collection<NodeDb> collection) {
		List<Node> nodes = new ArrayList<Node>();
		for (NodeDb nodeDb : collection) {
			Node node = new Node();
			node.setId(nodeDb.getId());
			node.setLat(nodeDb.getLat());
			node.setLon(nodeDb.getLon());
			nodes.add(node);
		}
		return nodes;
	}
}
