package com.example.trekkinggps.databases;

import java.util.ArrayList;
import java.util.List;

import jsqlite.Stmt;

import org.osmdroid.util.GeoPoint;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.trekkinggps.findroute.SearchLocationPoint;
import com.example.trekkinggps.findroute.model.Node;
import com.example.trekkinggps.findroute.model.Node.Color;

public class RouteDatabaseHelper extends DatabaseHelper {

	public RouteDatabaseHelper(Context context) {
		super(context);
	}

	public Cursor searchByInputText(String text) {
		String[] columns = new String[] { "location", "description", "lat", "lon" };
		String whereClause = "location match '" + text + "*'";
		Cursor cursor = myDataBase.query(true, "places", columns, whereClause, null, null, null, null, null);
		return cursor;
	}

	public SearchLocationPoint getDescriptionForCoordinates(double lat, double lon) {

		String newLat = String.valueOf(lat);
		if (newLat.length() > 4) {
			newLat = newLat.substring(0, 4);
		}

		String newLon = String.valueOf(lon);
		if (newLon.length() > 4) {
			newLon = newLon.substring(0, 4);
		}


		String[] columns = new String[] { "location", "description" };
		String whereClause = "places match 'lat:" + newLat + "* lon:" + newLon + "*'";
		String orderBy = "abs(lat - " + lat + ") + abs(lon - " + lon + ")";
		Cursor cursor = myDataBase.query(true, "places", columns, whereClause, null, null, null, orderBy, "1");
		SearchLocationPoint result = null;

		if (cursor.moveToFirst()) {
			result = new SearchLocationPoint(lat, lon);
			result.setLocation(cursor.getString(cursor.getColumnIndex("location")));
			result.setDescription(cursor.getString(cursor.getColumnIndex("description")).substring(2));
		}

		return result;
	}

	/**
	 * Returns matching point on trail for given <code>geoPoint</code> (from
	 * regular database).
	 * 
	 * @param geoPoint
	 * @return
	 */

	public Node getLocationOnTrailForPoint(GeoPoint geoPoint) {
		String wayIdQuery = "select id from(SELECT id , geometry , ST_Distance(geometry, MakePoint("
				+ geoPoint.getLongitude() + "," + geoPoint.getLatitude() + ")) AS distance  FROM " + MAIN_TABLE_NAME
				+ " WHERE distance < 0.05 and ROWID IN ( SELECT ROWID FROM SpatialIndex WHERE f_table_name = '" + MAIN_TABLE_NAME
				+ "' AND search_frame = BuildCircleMbr(" + geoPoint.getLongitude() + "," + geoPoint.getLatitude()
				+ " ,0.05)) order by distance limit 1)";

		Long wayId = null;

		try {
			Stmt stmt = db.prepare(wayIdQuery);

			if (stmt.step()) {
				wayId = stmt.column_long(0);
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String query = "select id, lon, lat,way_pos, ST_Distance(MakePoint(lon, lat), MakePoint(" + geoPoint.getLongitude()
				+ "," + geoPoint.getLatitude()
				+ ")) as distance from ways_nodes join nodes where node_id=nodes.id and way_id=" + wayId
				+ " order by distance limit 1 ";

		Node node = null;
		try {
			Stmt stmt = db.prepare(query);

			if (stmt.step()) {
				node = new Node();
				node.setId(stmt.column_long(0));
				node.setLon(stmt.column_double(1));
				node.setLat(stmt.column_double(2));
				node.setWayPos(stmt.column_int(3));
				node.setWayId(wayId);
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return node;
	}

	/**
	 * Returns matching point on trail for given <code>geoPoint</code> from
	 * routing database.
	 * 
	 * @param geoPoint
	 * @return
	 */
	public Node getLocationOnTrailForPointRouting(GeoPoint geoPoint, List<Node> pointsOnTrail) {
		Node closestNode = getLocationOnTrailForPoint(geoPoint);
		Node node = null;
		if(closestNode != null) {
			pointsOnTrail.add(closestNode);
			String query = "select node_from, nodes.lon, nodes.lat,way_id_from, way_pos_from, color,ST_Distance(MakePoint(nodes.lon,nodes.lat),  MakePoint("
					+ closestNode.getLon()
					+ ","
					+ closestNode.getLat()
					+ ")) as distance from routing2 join nodes on node_from=nodes.id where way_id_from="
					+ closestNode.getWayId() + " order by distance limit 1";

			try {
				Stmt stmt = db.prepare(query);
				if (stmt.step()) {
					node = new Node();
					node.setId(stmt.column_long(0));
					node.setLon(stmt.column_double(1));
					node.setLat(stmt.column_double(2));
					node.setWayId(stmt.column_long(3));
					node.setWayPos(stmt.column_int(4));
					node.setColor(Color.valueOf(stmt.column_string(5).toUpperCase()));
				}
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return node;
	}

	/**
	 * Returns list of points between given points (theirs positions on given
	 * trail - <code>way_id</code>)
	 * 
	 * @param way_pos1
	 * @param way_pos2
	 * @param way_id
	 * @param desc
	 * @return
	 */
	public List<Node> getWayPointsBeetweenTwoPoints(int way_pos1, int way_pos2, Long way_id, boolean desc) {

		String query = "select lat, lon,id, way_id from ways_nodes as wn inner join nodes as n on wn.node_id = n.id where way_id = "
				+ way_id + " and way_pos between " + way_pos1 + " and " + way_pos2 + " order by way_pos ";
		if (desc) {
			query += "DESC";
		}

		Cursor cursor = myDataBase.rawQuery(query, null);
		List<Node> points = null;
		if (cursor.moveToFirst()) {
			points = new ArrayList<Node>();
			Node node = new Node();
			node.setLat(cursor.getDouble(cursor.getColumnIndex("lat")));
			node.setLon(cursor.getDouble(cursor.getColumnIndex("lon")));

			points.add(node);

			while (cursor.moveToNext()) {
				node = new Node();
				node.setLat(cursor.getDouble(cursor.getColumnIndex("lat")));
				node.setLon(cursor.getDouble(cursor.getColumnIndex("lon")));
				points.add(node);
			}
		}
		return points;
	}

	/**
	 * Returns position on way with id <code>way_id</code> for
	 * <code>geoPoint</code>.
	 * 
	 * @param way_id
	 * @param point
	 * @return
	 */
	public int getWayPositionOfPointOnRoute(Long way_id, Node point) {
		String query = "select way_pos from ways_nodes where ";
		query += "way_id = " + way_id;
		query += " and node_id=  " + point.getId();

		Cursor cursor = myDataBase.rawQuery(query, null);
		int wayPos = -1;
		if (cursor.moveToFirst()) {
			wayPos = cursor.getInt(0);
		}
		return wayPos;
	}

	/**
	 * Estimates way position for <code>intersectionPoint</code> - intersection
	 * does not have this value in database.
	 * 
	 * @param intersectionPoint
	 * @param wayId
	 * @return
	 */
	public int getWayPositionOfIntersectionPoint(GeoPoint intersectionPoint, int wayId) {

		String query = "select avg(avg)  from (select sum(way_pos) as avg ,node_id, ST_Distance(MakePoint("
				+ intersectionPoint.getLongitude() + "," + intersectionPoint.getLatitude()
				+ "),MakePoint(lon,lat)) as distance from ways_nodes join nodes where node_id=nodes.id and way_id="
				+ wayId + " group by node_id order by distance  limit 2  ) ";
		int wayPos = -1;
		try {
			Stmt stmt = db.prepare(query);
			if (stmt.step()) {
				wayPos = stmt.column_int(0);
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return wayPos;
	}

	public List<Node> getNeighbors(Node node) {
		String query = "select node_from, node_to, distance, way_id_to, way_pos_to, lon_to, lat_to, color from routing2 where node_from="
				+ node.getId()
				+ " and way_id_from="
				+ node.getWayId();
		

		List<Node> neighbors = new ArrayList<Node>();
		Log.d("TrekkingGPS", "wyciagam sasiadow! dla " + node.getId() + " " + node.getWayId());

		try {
			Stmt stmt = db.prepare(query);
			while (stmt.step()) {
				Node nodeTo = new Node();
				nodeTo.setId(stmt.column_long(1));
				nodeTo.setDistance(stmt.column_double(2));
				nodeTo.setLat(stmt.column_double(6));
				nodeTo.setLon(stmt.column_double(5));
				nodeTo.setWayId(stmt.column_long(3));
				nodeTo.setWayPos(stmt.column_int(4));

				nodeTo.setColor(Color.valueOf(stmt.column_string(7).toUpperCase()));
				neighbors.add(nodeTo);
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// skrzyzowania, ktore nie istnieja jako ten sam punkt na dwoch szlakach
		query = "select node_from, node_to, distance, way_id_to, way_pos_to, lon_to, lat_to, color from routing3 where node_from="
				+ node.getId();

		try {
			Stmt stmt = db.prepare(query);
			while (stmt.step()) {
				Node nodeTo = new Node();
				nodeTo.setId(stmt.column_long(1));
				nodeTo.setDistance(stmt.column_double(2));
				nodeTo.setLat(stmt.column_double(6));
				nodeTo.setLon(stmt.column_double(5));
				nodeTo.setWayId(stmt.column_long(3));
				nodeTo.setWayPos(stmt.column_int(4));
				nodeTo.setColor(Color.valueOf(stmt.column_string(7).toUpperCase()));

				neighbors.add(nodeTo);
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Log.d("TrekkingGPS", "wyciagnalem: " + neighbors.size());
		return neighbors;
	}
}
