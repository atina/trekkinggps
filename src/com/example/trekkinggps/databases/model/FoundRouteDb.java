package com.example.trekkinggps.databases.model;

import java.io.Serializable;
import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "found_routes")
public class FoundRouteDb implements Serializable {

	@Override
	public String toString() {
		return name;
	}

	private static final long serialVersionUID = 1L;

	public FoundRouteDb() {

	}

	@DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	private FoundRouteDb foundRouteDb;
	@DatabaseField(canBeNull=true)
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getCenterLat() {
		return centerLat;
	}

	public void setCenterLat(double centerLat) {
		this.centerLat = centerLat;
	}

	public double getCenterLon() {
		return centerLon;
	}

	public void setCenterLon(double centerLon) {
		this.centerLon = centerLon;
	}

	public double getDividerLon() {
		return dividerLon;
	}

	public void setDividerLon(double dividerLon) {
		this.dividerLon = dividerLon;
	}

	public double getDividerLat() {
		return dividerLat;
	}

	public void setDividerLat(double dividerLat) {
		this.dividerLat = dividerLat;
	}

	@DatabaseField(canBeNull = true)
	private double centerLat;

	@DatabaseField(canBeNull = true)
	private double centerLon;

	@DatabaseField(canBeNull = true)
	private double dividerLat;

	@DatabaseField(canBeNull = true)
	private double dividerLon;

	@DatabaseField(generatedId = true)
	private Long id;
	@DatabaseField(unique = true, canBeNull = true)
	private String name;

	@ForeignCollectionField(eager = false)
	private Collection<NodeDb> fullPointsList;

	@ForeignCollectionField(eager = false)
	private Collection<FoundRouteDb> routeDesctiptionToDistance;



	@DatabaseField()
	private Double Length;

	public Collection<NodeDb> getFullPointsList() {
		return fullPointsList;
	}

	public void setFullPointsList(Collection<NodeDb> nodes) {
		this.fullPointsList = nodes;
	}

	
	public Double getLength() {
		return Length;
	}

	public void setLength(Double length) {
		Length = length;
	}

	public void setName(String name) {
		this.name = name;

	}

	public String getName() {
		return name;
	}

	public Collection<FoundRouteDb> getRouteDesctiptionToDistance() {
		return routeDesctiptionToDistance;
	}

	public void setRouteDesctiptionToDistance(
			Collection<FoundRouteDb> routeDesctiptionToDistance) {
		this.routeDesctiptionToDistance = routeDesctiptionToDistance;
	}

	public FoundRouteDb getFoundRouteDb() {
		return foundRouteDb;
	}

	public void setFoundRouteDb(FoundRouteDb foundRouteDb) {
		this.foundRouteDb = foundRouteDb;
	}

}
