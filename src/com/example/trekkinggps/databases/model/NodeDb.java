package com.example.trekkinggps.databases.model;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "nodesDb")
public class NodeDb implements Serializable {

	private static final long serialVersionUID = 1L;

	@DatabaseField(generatedId=true)
	private Long id;

	@DatabaseField
	private Double lon;

	@DatabaseField
	private Double lat;

	@DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
	private FoundRouteDb foundRouteDb;

	public FoundRouteDb getFoundRouteDb() {
		return foundRouteDb;
	}

	public void setFoundRouteDb(FoundRouteDb foundRouteDb) {
		this.foundRouteDb = foundRouteDb;
	}

	//Needed by ORMLite
	public NodeDb() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

}