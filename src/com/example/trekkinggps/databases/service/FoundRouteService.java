package com.example.trekkinggps.databases.service;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;

import com.example.trekkinggps.databases.DatabaseHelper;
import com.example.trekkinggps.databases.model.FoundRouteDb;
import com.example.trekkinggps.databases.model.NodeDb;
import com.example.trekkinggps.databases.translators.FoundRouteTranslator;
import com.example.trekkinggps.findroute.model.FoundRoute;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Database helper class used to manage the creation and upgrading of your
 * database. This class also usually provides the DAOs used by the other
 * classes.
 */
public class FoundRouteService {

	private Dao<FoundRouteDb, String> foundRouteDao;
	private Dao<NodeDb, String> nodeDao;
	private ConnectionSource connectionSource;
	private final Context context;

	public FoundRouteService(Context context) {
		this.context = context;
	}

	private void connectToDb() throws SQLException {
		connectionSource = new AndroidConnectionSource(new DatabaseHelper(
				context));

	}

	private Dao<FoundRouteDb, String> getDao() throws SQLException {
		if (foundRouteDao == null)
			foundRouteDao = DaoManager.createDao(connectionSource,
					FoundRouteDb.class);
//		TableUtils.dropTable(connectionSource, FoundRouteDb.class,true);
		TableUtils.createTableIfNotExists(connectionSource, FoundRouteDb.class);
		return foundRouteDao;
	}

	private Dao<NodeDb, String> getNodeDao() throws SQLException {
		if (nodeDao == null)
			nodeDao = DaoManager.createDao(connectionSource, NodeDb.class);
//		TableUtils.dropTable(connectionSource, NodeDb.class,true);
		TableUtils.createTableIfNotExists(connectionSource, NodeDb.class);
		return nodeDao;
	}

	public void save(FoundRoute foundRoute, String name) throws SQLException {
		FoundRouteTranslator translator = new FoundRouteTranslator();
		FoundRouteDb foundRouteDb = translator.translate(foundRoute);
		connectToDb();

		getNodeDao();
		getDao();

		for (NodeDb nodeDb : foundRouteDb.getFullPointsList())
			nodeDao.createOrUpdate(nodeDb);


		for (FoundRouteDb f : foundRouteDb.getRouteDesctiptionToDistance())
			foundRouteDao.createOrUpdate(f);

		foundRouteDb.setName(name);

		// persist object to the database
		foundRouteDao.createOrUpdate(foundRouteDb);

		closeConnection();
	}

	public int update(String oldName, String newName) throws SQLException {
		connectToDb();
		getDao();

		UpdateBuilder<FoundRouteDb, String> updateBuilder = foundRouteDao
				.updateBuilder();

		updateBuilder.updateColumnValue("name", newName);
		updateBuilder.where().eq("name", oldName);

		closeConnection();
		return updateBuilder.update();

	}

	public int delete(String name) throws SQLException {
		connectToDb();
		getDao();
		DeleteBuilder<FoundRouteDb, String> queryBuilder = foundRouteDao
				.deleteBuilder();
		queryBuilder.where().eq("name", name);
		closeConnection();
		return queryBuilder.delete();
	}

	public FoundRouteDb getFromDb(String name) throws SQLException {
		connectToDb();
		getDao();

		QueryBuilder<FoundRouteDb, String> queryBuilder = foundRouteDao
				.queryBuilder();
		queryBuilder.where().eq("name", name);
		closeConnection();
		return queryBuilder.query().get(0);

	}

	public List<FoundRouteDb> getAllFromDb() throws SQLException {
		connectToDb();
		getDao();
		QueryBuilder<FoundRouteDb, String> queryBuilder = foundRouteDao
				.queryBuilder();
		queryBuilder.where().isNotNull("name");

		closeConnection();
		return queryBuilder.query();
	}

	private void closeConnection() throws SQLException {
		connectionSource.close();
	}
}
