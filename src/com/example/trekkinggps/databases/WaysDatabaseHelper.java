package com.example.trekkinggps.databases;

import jsqlite.Exception;
import jsqlite.Stmt;
import android.content.Context;

public class WaysDatabaseHelper extends DatabaseHelper {

	public WaysDatabaseHelper(Context context) {
		super(context);
	}

	public StringBuilder getWayId(double latitude, double longtitude) {
		openSpatialite();

		String query = "SELECT id, name, route, ref, ST_Distance(geometry, MakePoint(" + longtitude + ", " + latitude + ")) AS distance FROM "
				+ MAIN_TABLE_NAME + " WHERE distance < 0.05 AND ROWID IN ( SELECT ROWID FROM SpatialIndex WHERE f_table_name = '" + MAIN_TABLE_NAME
				+ "' AND search_frame = BuildCircleMbr(" + longtitude + ", " + latitude + ", 0.05))ORDER BY distance limit 1;";

		StringBuilder sb = null;
		try {
			Stmt stmt = db.prepare(query);

			if (stmt.step()) {
				sb = new StringBuilder();
				int id = stmt.column_int(0);
				String name = stmt.column_string(1);
				String type = stmt.column_string(2);
				String color = stmt.column_string(3);

				sb.append("Identyfikator szlaku: ").append(id).append("\n");
				sb.append("Nazwa: ").append(name).append("\n");
				sb.append("Kolor: ").append(color).append("\n");
				sb.append("Typ: ").append(type).append("\n");

			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		closeSpatialite();

		return sb;
	}

}
