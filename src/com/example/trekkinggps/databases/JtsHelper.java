package com.example.trekkinggps.databases;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;

public class JtsHelper {
	static ThreadLocal<WKBReader> wkbReader = new ThreadLocal<WKBReader>() {
		@Override
		protected WKBReader initialValue() {
			return new WKBReader();
		};
	};

	public static Geometry bytesToGeometry(byte geometryBytes[]) throws ParseException {
		WKBReader reader = wkbReader.get();
		return reader.read(geometryBytes);
	}
}