package com.example.trekkinggps.tiles;

import android.app.Activity;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.example.trekkinggps.R;
import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IMapView;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.safecanvas.ISafeCanvas;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ewelina on 2015-01-04.
 */
public class RouteDescriptionOverlay extends ItemizedOverlay<OverlayItem> {

    private final Activity context;
    // lista "itemow" - punktow (obrazki) na warstwie
    private ArrayList<OverlayItem> overlayItemList = new ArrayList<OverlayItem>();


    public RouteDescriptionOverlay(Drawable pDefaultMarker, ResourceProxy pResourceProxy, Activity context) {
        super(pDefaultMarker, pResourceProxy);
        this.context = context;
    }

    public void addDescription(String color, String time, GeoPoint descriptionPoint) {
        OverlayItem overlayItem = new OverlayItem(color, time, descriptionPoint);
        overlayItemList.add(overlayItem);
        populate();
    }

    // dopisuje czas na obrazku fragmentu szlaku
    public BitmapDrawable writeOnDrawable(int drawableId, String text) {

        Bitmap bm = BitmapFactory.decodeResource(context.getResources(),
                drawableId).copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        paint.setShadowLayer(0.5f, 1, 1, Color.GRAY);
        paint.setTextSize(12);

        Canvas canvas = new Canvas(bm);
        canvas.drawText(text, 0, bm.getHeight(), paint);

        return new BitmapDrawable(context.getResources(), bm);
    }

    // nadpisana, zeby malowalo obrazki dla fragmentów szlaku z czasowkami
    @Override
    protected void onDrawItem(ISafeCanvas canvas, OverlayItem item,
                              Point curScreenCoords, float aMapOrientation) {

        int drawableId = getDrawableIdFromColor(item.getTitle());
        if(drawableId != 0) {
            BitmapDrawable drawable = writeOnDrawable(drawableId, item.getSnippet());
            item.setMarker(drawable);
            super.onDrawItem(canvas, item, curScreenCoords, aMapOrientation);
        }

    }

    private int getDrawableIdFromColor(String color) {
        switch (color) {
            case "RED":
                return R.drawable.ic_circle_red;
            case "BLUE":
                return R.drawable.ic_circle_blue;
            case "BLACK":
                return R.drawable.ic_circle_black;
            case "YELLOW":
                return R.drawable.ic_circle_yellow;
            case "PURPLE":
                return R.drawable.ic_circle_purple;
            case "GREEN":
                return R.drawable.ic_circle_green;
            case "MULTI":
                return R.drawable.ic_circle_multi;

        }
        return 0;
    }

    @Override
    protected OverlayItem createItem(int i) {
        return overlayItemList.get(i);
    }

    @Override
    public int size() {
        return overlayItemList.size();
    }

    @Override
    public boolean onSnapToItem(int i, int i1, Point point, IMapView iMapView) {
        return false;
    }
}
