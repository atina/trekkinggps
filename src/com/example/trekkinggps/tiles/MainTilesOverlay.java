package com.example.trekkinggps.tiles;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.views.MapView;
import org.osmdroid.views.MapView.Projection;
import org.osmdroid.views.overlay.TilesOverlay;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.trekkinggps.activities.MainActivity;
import com.example.trekkinggps.databases.WaysDatabaseHelper;

/**
 * klasa rysujaca szlaki na mapie w startowym activity oraz obsługująca
 * interaktywnosc
 */
public class MainTilesOverlay extends TilesOverlay {

	private final WaysDatabaseHelper wt;
	private final Context context;
	private boolean interactivityEnabled;

	public MainTilesOverlay(MapTileProviderBase aTileProvider,
			Context aContext, WaysDatabaseHelper wt) {
		super(aTileProvider, aContext);
		this.wt = wt;
		this.context = aContext;
		this.interactivityEnabled = true;
	}

	public void setInteractivityMode(boolean isEnabled) {
		this.interactivityEnabled = isEnabled;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e, MapView mapView) {
		if (interactivityEnabled) {
			Projection projection = mapView.getProjection();
			IGeoPoint loc = projection.fromPixels((int) e.getX(),
					(int) e.getY());

			StringBuilder result = wt.getWayId(loc.getLatitude(),
					loc.getLongitude());

			if (result != null) {
				MainActivity.toast = Toast.makeText(context, result,
						Toast.LENGTH_SHORT);
				MainActivity.toast.show();
			} else {
				Log.d("TAP CONFIRMED",
						String.valueOf(loc.getLatitude()) + loc.getLongitude());
			}
		}

		return super.onSingleTapConfirmed(e, mapView);
	}
}