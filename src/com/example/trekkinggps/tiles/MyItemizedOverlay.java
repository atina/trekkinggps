package com.example.trekkinggps.tiles;

import java.util.ArrayList;
import java.util.List;

import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapView;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.safecanvas.ISafeCanvas;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.example.trekkinggps.R;
import com.example.trekkinggps.activities.FindRouteActivity;
import com.example.trekkinggps.databases.RouteDatabaseHelper;
import com.example.trekkinggps.findroute.PointsAdapter;
import com.example.trekkinggps.findroute.SearchLocationPoint;

public class MyItemizedOverlay extends ItemizedOverlay<OverlayItem> implements
		ListView.OnItemClickListener, Button.OnClickListener {

	// lista "itemow" - punktow (obrazki) na warstwie
	private ArrayList<OverlayItem> overlayItemList = new ArrayList<OverlayItem>();
	// lista punktow na dole aplikacji
	private List<GeoPoint> pointsList = new ArrayList<GeoPoint>();
	private Activity context;
	private ListView pointsView;
	private ImageButton pointsListHide;
	private ImageButton fullscreen;
	boolean fullscreenOn = false;
	private MapView mapView;
	// przechowuje info, czy aktualnie jest dodany punkt z bieżącą lokalizacją
	boolean gpsPointAdded = false;
	// ta zmienna jest ustawiana na true jesli wybierzemy opcję move dla punktu
	// - pozwala na dodanie punktu w innym miejscu i usuniecie starego (czyli
	// przemieszczenie go)
	boolean onSingleTapEnabled = false;
	int temporaryPointIndex;
	// tryby edycji i dodawania punktow (wlaczone/wylaczone)
	private boolean addingPointsMode = false;
	private boolean editingPointsMode = false;
	private final RouteDatabaseHelper database;

	public MyItemizedOverlay(Drawable pDefaultMarker,
			ResourceProxy pResourceProxy, Activity context,
			ListView pointsView, MapView mapView) {
		super(pDefaultMarker, pResourceProxy);
		this.context = context;
		this.pointsView = pointsView;
		pointsView.setOnItemClickListener(this);
		pointsListHide = (ImageButton) context.findViewById(R.id.hide_button2);
		pointsListHide.setOnClickListener(this);
		fullscreen = (ImageButton) context.findViewById(R.id.full_screen);
		fullscreen.setOnClickListener(this);
		this.mapView = mapView;
		database = new RouteDatabaseHelper(context);

	}

	// dodaje punkt po kliknieciu na mape/wyszukaniu lokalizacji
	public void addItem(GeoPoint p, String title, String snippet) {
		OverlayItem newItem = new OverlayItem(title, snippet, p);
		pointsList.add(p);
		if (pointsListHide.getVisibility() == View.GONE) {
			pointsListHide.setVisibility(View.VISIBLE);
			fullscreen.setVisibility(View.VISIBLE);
		}
		changePointsList();
		overlayItemList.add(newItem);
		populate();
	}

	// uzywana przy dodawaniu punktu gps (na pierwszej pozycji) lub przy zmianie
	// kolejnosci (dodawanie na x-tej pozycji i usuwanie ze starej)
	public void addItemAtPosition(GeoPoint geoPoint, int index,
			boolean isGpsPoint) {
		OverlayItem newItem = new OverlayItem("point", "point", geoPoint);
		if (gpsPointAdded || !isGpsPoint) {
			pointsList.remove(index);
			overlayItemList.remove(index);
		}

		if (isGpsPoint) {
			gpsPointAdded = true;
		}

		pointsList.add(index, geoPoint);
		if (pointsListHide.getVisibility() == View.GONE) {
			pointsListHide.setVisibility(View.VISIBLE);
			fullscreen.setVisibility(View.VISIBLE);
		}
		overlayItemList.add(index, newItem);
		changePointsList();

		populate();
	}

	// usuwanie punktu-biezacej lokalizacji
	public void removeItemFromFirstPosition() {
		pointsList.remove(0);
		if (pointsList.isEmpty()) {
			pointsListHide.setVisibility(View.GONE);
			fullscreen.setVisibility(View.GONE);
		}
		if (pointsList.size() < 5) {
			RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) pointsView
					.getLayoutParams();
			layoutParams.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
			pointsView.setLayoutParams(layoutParams);
		}
		overlayItemList.remove(0);
		gpsPointAdded = false;

		changePointsList();
		populate();
	}

	public List<GeoPoint> getPoints() {
		return this.pointsList;
	}

	// metoda uzywana przy wywolaniu move dla punku - klikamy na jego nowa
	// lokalizacje na mapie (ustawia jego index i blokuje mozliwosc edycji
	// chwilowo)
	public void enableSingleTapOnce(int index) {
		temporaryPointIndex = index;
		this.onSingleTapEnabled = true;
		this.editingPointsMode = false;
	}

	public void enableAddingPoints() {
		this.addingPointsMode = true;
	}

	public void disableAddingPoints() {
		this.addingPointsMode = false;
	}

	public void enableEditingPoints() {
		this.editingPointsMode = true;
	}

	public void disableEditingPoints() {
		this.editingPointsMode = false;
	}

	// odswieza liste punktow
	public void changePointsList() {
		List<String> points = new ArrayList<String>();
		for (int i = 0; i < pointsList.size(); i++) {
			String pointDesc = null;
			if (pointsList.get(i) instanceof SearchLocationPoint) {
				pointDesc = (i + 1)
						+ ". "
						+ "<b>" + ((SearchLocationPoint) pointsList.get(i)).getLocation() + "</b><br/>"
						+ "<i>" + ((SearchLocationPoint) pointsList.get(i)).getDescription() + "</i>";
			} else {
				double lat = pointsList.get(i).getLatitude();
				double lon = pointsList.get(i).getLongitude();
				pointDesc = (i + 1) + ". " + lat + ", " + lon;
			}

			points.add(pointDesc);
		}
		if (points.size() > 4 && !fullscreenOn) {
			RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) pointsView
					.getLayoutParams();
			layoutParams.height = 200;
			pointsView.setLayoutParams(layoutParams);
		}
		pointsView.setAdapter(new PointsAdapter(context, points, this));
	}

	// dopisuje numerek na obrazku punktu
	public BitmapDrawable writeOnDrawable(int drawableId, String text) {

		Bitmap bm = BitmapFactory.decodeResource(context.getResources(),
				drawableId).copy(Bitmap.Config.ARGB_8888, true);

		Paint paint = new Paint();
		paint.setStyle(Style.FILL);
		paint.setColor(Color.BLACK);
		paint.setShadowLayer(0.5f, 1, 1, Color.GRAY);
		paint.setTextSize(20);

		Canvas canvas = new Canvas(bm);
		canvas.drawText(text, bm.getWidth() / 3, bm.getHeight() / 2, paint);

		return new BitmapDrawable(context.getResources(), bm);
	}

	// nadpisana, zeby malowalo punkty z dodanymi numerkami na nich
	@Override
	protected void onDrawItem(ISafeCanvas canvas, OverlayItem item,
			Point curScreenCoords, float aMapOrientation) {
		int itemIdx = pointsList.indexOf(item.getPoint()) + 1;
		BitmapDrawable drawable = writeOnDrawable(R.drawable.ic_point_blue,
				String.valueOf(itemIdx));
		item.setMarker(drawable);
		super.onDrawItem(canvas, item, curScreenCoords, aMapOrientation);
	}

	@Override
	public boolean onSnapToItem(int arg0, int arg1, Point arg2, IMapView arg3) {
		return false;
	}

	@Override
	protected OverlayItem createItem(int arg0) {
		// TODO Auto-generated method stub
		return overlayItemList.get(arg0);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return overlayItemList.size();
	}

	// okienko pojawiajace sie po wybraniu opcji change position dla punktu - ma
	// drop down z listą pozycji
	public void changePosition(int index, int shift) {

		GeoPoint geoPoint = pointsList.get(index);
		pointsList.remove(index);
		pointsList.add(index + shift, geoPoint);

		OverlayItem item = overlayItemList.get(index);
		overlayItemList.remove(index);
		overlayItemList.add(index + shift, item);

		populate();
		mapView.invalidate();
		changePointsList();

	}

	public void remove(int index) {
		pointsList.remove(index);
		if (pointsList.isEmpty()) {
			pointsListHide.setVisibility(View.GONE);
			fullscreen.setVisibility(View.GONE);
		}
		if (pointsList.size() < 5) {
			RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) pointsView
					.getLayoutParams();
			layoutParams.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
			pointsView.setLayoutParams(layoutParams);
		}
		overlayItemList.remove(index);
		populate();
		mapView.invalidate();
		changePointsList();
	}

	// ta metoda jest wywolywana po dotknieciu punktu na mapie
	@Override
	protected boolean onTap(int index) {
		if (editingPointsMode) {
			// getPopup(index).show();
			FindRouteActivity.toast = Toast.makeText(context, "Point " + (index + 1)
					+ " selected.\nClick on new point location.",
					Toast.LENGTH_LONG);
			FindRouteActivity.toast.setGravity(Gravity.TOP, 0, 60);
			FindRouteActivity.toast.show();

			enableSingleTapOnce(index);
		}
		return super.onTap(index);
	}

	// metoda wywolywana po kliknieciu na punkt na liscie
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		mapView.getController().animateTo(pointsList.get(arg2));
	}

	// metoda wywolywana po dotknieciu na mape - dodaje punkty w trybie
	// dodawania lub podczas "move"
	@Override
	public boolean onSingleTapUp(MotionEvent arg0, MapView arg1) {
		if (onSingleTapEnabled || addingPointsMode) {
			if(addingPointsMode) {
				onSingleTapEnabled = false;
			}
			org.osmdroid.views.MapView.Projection projection = arg1
					.getProjection();
			IGeoPoint gp = projection.fromPixels((int) arg0.getX(),
					(int) arg0.getY());

			database.openDataBase();
			SearchLocationPoint pointWiLocation = database
					.getDescriptionForCoordinates(gp.getLatitude(),
							gp.getLongitude());
			database.close();

			GeoPoint geoPoint = (null != pointWiLocation ? pointWiLocation
					: new GeoPoint(gp.getLatitude(), gp.getLongitude()));
			if (onSingleTapEnabled) {
				addItemAtPosition(geoPoint, temporaryPointIndex, false);
				onSingleTapEnabled = false;
				editingPointsMode = true;
			} else {
				addItem(geoPoint, "point", "point");
			}

			mapView.invalidate();
		}
		return false;
	}

	// klikniecie na chowanie sie listy
	@Override
	public void onClick(View v) {

		RelativeLayout.LayoutParams pointsViewParams = (LayoutParams) pointsView
				.getLayoutParams();
		RelativeLayout.LayoutParams pointListHideParams = (RelativeLayout.LayoutParams) pointsListHide
				.getLayoutParams();
		RelativeLayout.LayoutParams fullscreenParams = (RelativeLayout.LayoutParams) fullscreen
				.getLayoutParams();

		if (v == pointsListHide) {

			if (pointsView.getVisibility() == View.VISIBLE) {
				pointListHideParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				fullscreenParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				pointsView.setVisibility(View.GONE);
				pointsListHide.setImageResource(R.drawable.ic_hide_up);
			} else {
				pointListHideParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
						0);
				fullscreenParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
				pointsView.setVisibility(View.VISIBLE);
				if (pointsList.size() > 4) {
					pointsViewParams.height = 200;
				} else {
					pointsViewParams.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
				}
				pointsListHide.setImageResource(R.drawable.ic_hide_down);
			}

			pointsListHide.setLayoutParams(pointListHideParams);
			fullscreen.setImageResource(R.drawable.ic_fullscreen);
			fullscreen.setLayoutParams(fullscreenParams);
			fullscreenOn = false;

		} else if (v == fullscreen) {

			if (fullscreenOn) {
				fullscreen.setImageResource(R.drawable.ic_fullscreen);

				if (pointsList.size() > 4) {
					pointsViewParams.height = 200;
				} else {
					pointsViewParams.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
				}
				pointsView.setLayoutParams(pointsViewParams);
				fullscreenOn = false;
			} else {
				fullscreen.setImageResource(R.drawable.ic_fullscreen_out);
				pointsViewParams.height = 870;
				pointsView.setLayoutParams(pointsViewParams);
				fullscreenOn = true;
				pointListHideParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
						0);
				fullscreenParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
				pointsView.setVisibility(View.VISIBLE);
				pointsListHide.setImageResource(R.drawable.ic_hide_down);

			}
		}

	}

}